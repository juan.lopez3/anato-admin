<?php

	$ruta = $_SERVER['DOCUMENT_ROOT'] . "/tfi/documents";
//print_r($_POST); return;

	switch ($_POST['operation']) {
		case 'insert':
			echo insertDocument($ruta, $_FILES);
			break;

		case 'edit':
			echo editDocument($ruta, $_POST['old'], $_POST['new']);
			break;

		case 'download':
			echo downloadDocument($ruta, $_POST['urlfile']);
			break;

		case 'delete':
			echo deleteDocument($ruta, $_POST['name']);
			break;
	}

	function insertDocument($ruta, $FILES){

		//Verifica si existe la carpeta, sino la crea.
		if (!is_dir($ruta)){
			if ( !mkdir($ruta, 0777) ){
				return "server";
			}
		}

		//Verifica si se puede guardar archivos en $ruta.
		if(!is_writable($ruta)) {
			return "server";
		}

		foreach ($FILES as $file) {
			$temporal = $file['tmp_name'];
			$path = $ruta."/";

			$filename = preg_replace('/[^a-z0-9-_\-\.]/i','_',$file['name']);
			$name = substr($filename,0,-4);
			$extension = substr($filename,-4,4);

			//Comprueba que sea un PDF
			if ( $extension != ".pdf" ) {
				return "pdf";
			}

			//Comprueba si el archivo existe; si existe le cambia el nombre, sino lo deja como esta.
			if ( file_exists($path.$filename) ) {
				$counter = 1;

				while ( file_exists($path.$name."_".$counter.$extension) ) {
					++$counter;
				}

				$name .= "_" . $counter;
				$filename = $name.$extension;
				
			} else {
				$filename = $name.$extension;
			}

			//Guarda el archivo en la carpeta $ruta.
			if ( move_uploaded_file($temporal, $path.$filename) ){
				return "http://mocionsoft.com/tfi/documents/".$filename;

			} else {
				return "server";				

			}

		}
	
	}

	//Metodo no usuado actualmente.
	function editDocument($ruta, $oldName, $newName){
		$oldPath = $ruta . "/" . $oldName;
		$newPath = $ruta . "/" . $newName;

		if (rename($oldPath, $newPath)) {
			return true;
		} else {
			return false;
		}

	}

	function deleteDocument($ruta, $name){
		$path = $ruta . "/" . $name;

		if (unlink($path)) {
			return true;
		} else {
			return false;
		}

	}

?>