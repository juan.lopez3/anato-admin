<?php 

	$ruta = $_SERVER['DOCUMENT_ROOT'] . "/tfi/images";
	
	//Verifica si existe la carpeta, sino la crea.
	if (!is_dir($ruta) ){
		if ( !mkdir($ruta, 0777) ){
			return;
		}
	}

	//Verifica si se puede guardar archivos en $ruta.
	if(!is_writable($ruta)) {
		return;
	}

	foreach ($_FILES as $file) {
		$temporal = $file['tmp_name'];
		$path = $ruta."/";

		$filename = preg_replace('/[^a-z0-9-_\-\.]/i','_',$file['name']);
		$name = substr($filename,0,-4);
		$extension = substr($filename,-4,4);

		//Comprueba si el archivo existe; si existe le cambia el nombre, sino lo deja como esta.
		if ( file_exists($path.$filename) ) {
			$counter = 1;

			while ( file_exists($path.$name."_".$counter.$extension) ) {
				++$counter;
			}

			$name .= "_" . $counter;
			$filename = $name.$extension;
			
		} else {
			$filename = $name.$extension;
		}

		//Guarda el archivo en la carpeta $ruta.
		if ( move_uploaded_file($temporal, $path.$filename) ){
			/*
			$type = pathinfo($path.$filename, PATHINFO_EXTENSION);
			$data = file_get_contents($path.$filename);
			$base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
			echo $base64;
			*/
			echo "http://mocionsoft.com/tfi/images/".$filename;
			return;

		} else {
			return;

		}

	}

?>