/*

	Modulo solo para elementos de configuracion

*/

var config = angular.module('config', [])

config.config(function($stateProvider, $urlRouterProvider) {

	$urlRouterProvider.when('', '/admin/types');

	$stateProvider

  .state('login', {
    url: '/login',
    templateUrl: 'app/login/views/index.tpl.html'
  })

  /*
	  	Main
	  --------------------------------------------------------- */
    .state('admin', {
    url: '/admin',
    templateUrl: 'app/admin/views/index.admin.tpl.html'
  })

  /*
	  	Settings
	  --------------------------------------------------------- */
  .state('admin.settings', {
    url: '/settings',
    views: {
      'adminView': {
        templateUrl: 'app/admin/settings/views/main.tpl.html'
      }
    }
  })

	/*
	  	Events
	  --------------------------------------------------------- */
		.state('admin.events', {
		url: '/events/:idType',
		views: {
			'adminView': {
				templateUrl: 'app/admin/events/views/events.tpl.html'
			}
		}
	})
		.state('admin.editEvent', {
		url: '/:idType/edit-event/:idEvent',
		views: {
			'adminView': {
				templateUrl: 'app/admin/events/views/edit-event.tpl.html'
			}
		}
	})

	/*
	  	Programme
	  --------------------------------------------------------- */
		.state('admin.programme', {
		url: '/edit-event/:idEvent/schedule-event',
		views: {
			'adminView': {
				templateUrl: 'app/admin/programme/views/index.programme.tpl.html'
			}
		}
	})
		.state('admin.ScheduleAdd', {
		url: '/edit-event/:idEvent/schedule-event/add/{type}',
		views: {
			'adminView': {
				templateUrl: 'app/admin/programme/views/add.programme.tpl.html'
			}
		}
	})
		.state('admin.ScheduleEdit', {
		url: '/edit-event/:idEvent/schedule-event/edit/:idProgramme/{type}',
		views: {
			'adminView': {
				templateUrl: 'app/admin/programme/views/edit.programme.tpl.html'
			}
		}
	})

	/*
	  	Speakers
	  --------------------------------------------------------- */
		.state('admin.speakers', {
		url: '/speakers',
		views: {
			'adminView': {
				templateUrl: 'app/admin/speakers/views/index.tpl.html'
			}
		}
	})

		.state('admin.speakerAdd', {
		url: '/edit-event/:idEvent/schedule-event/edit/:idProgramme/add-speaker',
		views: {
			'adminView': {
				templateUrl: 'app/admin/speakers/views/add.speaker.tpl.html'
			}
		}
	})

		.state('admin.speakerEdit', {
		url: '/edit-event/edit-speaker/:idSpeaker',
		views: {
			'adminView': {
				templateUrl: 'app/admin/speakers/views/edit.speaker.tpl.html'
			}
		}
	})

	/*
	  	Documents
	  --------------------------------------------------------- */
		.state('admin.documents', {
		url: '/documents',
		views: {
			'adminView': {
				templateUrl: 'app/admin/documents/views/docs.index.tpl.html',
				controller: 'indexDocsCtrl'
			}
		}
	})

		.state('admin.viewDoc', {
		url: '/docs/:url',
		views: {
			'adminView': {
				templateUrl: 'app/admin/documents/views/doc.view.tpl.html',
				controller: 'viewDocCtrl'
			}
		}
	})

	/*
	  	Awards
	  --------------------------------------------------------- */
		.state('admin.awards', {
		url: '/awards',
		views: {
			'adminView': {
				templateUrl: 'app/admin/awards/views/index.awards.tpl.html'
			}
		}
	})
		.state('admin.awardsAdd', {
		url: '/awards/add',
		views: {
			'adminView': {
				templateUrl: 'app/admin/awards/views/add.awards.tpl.html'
			}
		}
	})
		.state('admin.awardsEdit', {
		url: '/awards/edit/:idAward',
		views: {
			'adminView': {
				templateUrl: 'app/admin/awards/views/edit.awards.tpl.html'
			}
		}
	})

  /*
	  	Sponsors
	  --------------------------------------------------------- */
    .state('admin.sponsors', {
    url: '/sponsors',
    views: {
      'adminView': {
        templateUrl: 'app/admin/spons/views/index.sponsors.tpl.html'
      }
    }
  })
    .state('admin.sponsorsEdit', {
    url: '/sponsors/edit/:idSponsor',
    views: {
      'adminView': {
        templateUrl: 'app/admin/spons/views/edit.sponsors.tpl.html'
      }
    }
  })

  /*
	  	Exhibitor
	  --------------------------------------------------------- */
    .state('admin.exhibitors', {
    url: '/:idEvent/exhibitors',
    views: {
      'adminView': {
        templateUrl: 'app/admin/exhibitor/views/index.exhibitor.tpl.html'
      }
    }
  })
    .state('admin.exhibitorEdit', {
    url: '/exhibitors/edit/:idExhibitor',
    views: {
      'adminView': {
        templateUrl: 'app/admin/exhibitor/views/edit.exhibitor.tpl.html'
      }
    }
  })
    .state('admin.exhibitorFile', {
    url: '/:idEvent/exhibitors/file',
    views: {
      'adminView': {
        templateUrl: 'app/admin/exhibitor/views/file.tpl.html'
      }
    }
  })

	/*
	  	FAQS
	  --------------------------------------------------------- */
		.state('admin.faqs', {
		url: '/:idEvent/faqs',
		views: {
			'adminView': {
				templateUrl: 'app/admin/FAQS/views/index.faqs.tpl.html'
			}
		}
	})
		.state('admin.faqsEdit', {
		url: '/:idEvent/faqs/edit/:idFAQ',
		views: {
			'adminView': {
				templateUrl: 'app/admin/FAQS/views/edit.faqs.tpl.html'
			}
		}
	})

	/*
	  	PFS
	  --------------------------------------------------------- */
		.state('admin.pfs', {
		url: '/pfs',
		views: {
			'adminView': {
				templateUrl: 'app/admin/pfs/views/index.pfs.tpl.html'
			}
		}
	})

	/*
	  	CPD
	  --------------------------------------------------------- */
		.state('admin.cpd', {
		url: '/cpd',
		views: {
			'adminView': {
				templateUrl: 'app/admin/cpd/views/index.tpl.html'
			}
		}
	})

	/*
	  	Terms
	  --------------------------------------------------------- */
		.state('admin.terms', {
		url: '/terms',
		views: {
			'adminView': {
				templateUrl: 'app/admin/terms/views/index.tpl.html'
			}
		}
	})

	/*
	  	Rates
	  --------------------------------------------------------- */
		.state('admin.rates', {
		url: '/:idEvent/rates',
		views: {
			'adminView': {
				templateUrl: 'app/admin/rates/views/index.tpl.html'
			}
		}
	})

	/*
	  	planos
	  --------------------------------------------------------- */
		.state('admin.floormap', {
		url: '/:idEvent/floormap',
		views: {
			'adminView': {
				templateUrl: 'app/admin/planos/views/index.tpl.html'
			}
		}
	})

	/*
	  	services
	  --------------------------------------------------------- */
		.state('admin.services', {
		url: '/:idEvent/services',
		views: {
			'adminView': {
				templateUrl: 'app/admin/services/views/index.tpl.html'
			}
		}
	})

	/*
	  	info general
	  --------------------------------------------------------- */
		.state('admin.infoGeneral', {
		url: '/:idEvent/infoGeneral',
		views: {
			'adminView': {
				templateUrl: 'app/admin/info-general/views/index.tpl.html'
			}
		}
	})


	/*
	  	Twitter
	  --------------------------------------------------------- */
		.state('admin.twitter', {
		url: '/twitter',
		views: {
			'adminView': {
				templateUrl: 'app/admin/twitter/views/index.tpl.html',
				controller: 'twitterCtrl'
			}
		}
	})


	/*
	  	Mapas
	  --------------------------------------------------------- */
		.state('admin.map', {
		url: '/edit-event/:idEvent/map/{lat}/{long}/{zoom}',
		views: {
			'adminView': {
				templateUrl: 'app/admin/maps/views/map.tpl.html',
				controller: 'mapsCtrl'
			}
		}
	})

	/*
	  	Acomodation
	  --------------------------------------------------------- */
		.state('admin.acomodation', {
		url: '/edit-event/:idEvent/acomodation',
		views: {
			'adminView': {
				templateUrl: 'app/admin/acomodation/views/index.tpl.html'
			}
		}
	})
		.state('admin.acomodationAdd', {
		url: '/edit-event/:idEvent/acomodation/add',
		views: {
			'adminView': {
				templateUrl: 'app/admin/acomodation/views/add.tpl.html'
			}
		}
	})
		.state('admin.acomodationEdit', {
		url: '/edit-event/:idEvent/acomodation/edit/:idAcomodation',
		views: {
			'adminView': {
				templateUrl: 'app/admin/acomodation/views/edit.tpl.html'
			}
		}
	})

	/*
	  	Venues
	  --------------------------------------------------------- */
		.state('admin.venueAdd', {
		url: '/edit-event/:idEvent/venue/add',
		views: {
			'adminView': {
				templateUrl: 'app/admin/venues/views/add.tpl.html'
			}
		}
	})
		.state('admin.venueEdit', {
		url: '/edit-event/:idEvent/venue/:idVenue',
		views: {
			'adminView': {
				templateUrl: 'app/admin/venues/views/edit.tpl.html'
			}
		}
	})
		.state('admin.location', {
		url: '/edit-event/:idEvent/venue/:idVenue/location',
		views: {
			'adminView': {
				templateUrl: 'app/admin/venues/views/location.tpl.html'
			}
		}
	})
		.state('admin.facilities', {
		url: '/edit-event/:idEvent/venue/:idVenue/facilities',
		views: {
			'adminView': {
				templateUrl: 'app/admin/venues/views/facilities.tpl.html'
			}
		}
	})
		.state('admin.directions', {
		url: '/edit-event/:idEvent/venue/:idVenue/directions',
		views: {
			'adminView': {
				templateUrl: 'app/admin/venues/views/directions.tpl.html'
			}
		}
	})
		.state('admin.dirAdd', {
		url: '/edit-event/:idEvent/venue/:idVenue/directions/add',
		views: {
			'adminView': {
				templateUrl: 'app/admin/venues/views/add.dir.tpl.html'
			}
		}
	})
	.state('admin.dirEdit', {
		url: '/edit-event/:idEvent/venue/:idVenue/directions/edit/:idDir',
		views: {
			'adminView': {
				templateUrl: 'app/admin/venues/views/edit.dir.tpl.html'
			}
		}
	})

  /*
	  	Polls
	  --------------------------------------------------------- */
      .state('admin.polls', {
      url: '/edit-event/:idEvent/polls',
    views: {
      'adminView': {
        templateUrl: 'app/admin/polls/views/index.tpl.html'
      }
    }
  })
    .state('admin.editPoll', {
    url: '/edit-event/:idEvent/edit-poll/:idPoll',
    views: {
      'adminView': {
        templateUrl: 'app/admin/polls/views/edit.tpl.html'
      }
    }
  })

  /*
	  	Polls
	  --------------------------------------------------------- */
  .state('admin.instagram', {
    url: '/edit-event/:idEvent/instagram',
    views: {
      'adminView': {
        templateUrl: 'app/admin/instagram/views/index.tpl.html'
      }
    }
  })

  /*
	  	Gallery
	  --------------------------------------------------------- */
  .state('admin.gallery', {
    url: '/edit-event/:idEvent/gallery',
    views: {
      'adminView': {
        templateUrl: 'app/admin/gallery/views/index.tpl.html'
      }
    }
  })

      /*
	  	Usuarios
	  --------------------------------------------------------- */
  .state('admin.users', {
    url: '/edit-event/users',
    views: {
      'adminView': {
        templateUrl: 'app/admin/users/views/index.tpl.html'
      }
    }
  })
  
  .state('admin.notifications', {
    url: '/notifications',
    views: {
      'adminView': {
        templateUrl: 'app/admin/notifications/views/index.tpl.html'
      }
    }
  })


}) //end config



