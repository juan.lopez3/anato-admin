angular
	.module('venues')

	.controller('dirCtrl', [
		'$state', '$stateParams', '$firebaseArray', 'firebaseURL', '$ionicLoading',
		function ($state, $stateParams, $firebaseArray, firebaseURL, $ionicLoading) {
			var ctrl = this;

			$ionicLoading.show({
				template: 'Loading directions'
			});

			//path to event's directions
			var path = firebaseURL + '/venues/' + $stateParams.idVenue + '/directions';
			//sync to firebase event's directions
			var sync = new Firebase(path);
			//data binding
			ctrl.directions = $firebaseArray(sync);

			//check object has been loaded
			ctrl.directions
				.$loaded()
				.then(function(data){
					$ionicLoading.hide();
				});

			ctrl.edit = function(id) {
				$state.go('admin.dirEdit', {
					idEvent: $stateParams.idEvent,
					idVenue: $stateParams.idVenue,
					idDir: id
				});
			}

			ctrl.add = function() {
				$state.go('admin.dirAdd', {
					idEvent: $stateParams.idEvent,
					idVenue: $stateParams.idVenue
				});
			}
		}
	])
