angular
	.module('venues')

	.controller('venueEditCtrl', [
  '$state','$stateParams', 'firebaseURL', '$firebaseObject', '$firebaseArray', '$ionicLoading', 'Upload', 'SERVER_IMAGES',
  function ($state,$stateParams, firebaseURL, $firebaseObject, $firebaseArray, $ionicLoading, Upload, SERVER_IMAGES) {

			var ctrl = this;

			//show loading while data it's retrieve
			$ionicLoading.show({
				template: 'retrieving venue data'
			});

			//path to selectec venue
			var path = firebaseURL + 'venues/' + $stateParams.idVenue;
			//sync to firebase event's schedule
			var sync = new Firebase(path);
			//three way data binding
			ctrl.venue = $firebaseObject(sync);

			//wait for data load
			ctrl.venue
				.$loaded()
				.then(function(data){
					$ionicLoading.hide();
				});

			ctrl.image = function(files){
            	if (!files || files[0] == undefined){ return; }

            	$ionicLoading.show({ template: 'Uploading Image ...' });

	            var file = files[0];
	            Upload.upload({
                  url: SERVER_IMAGES,
	                file: file
	            }).progress(function (evt) { })
	            .success(function (data, status, headers, config) {
					ctrl.venue.general.image = data;
					$ionicLoading.hide();

	            }).error(function (data, status, headers, config) {
	                console.log('Error status: ' + status);
	            });
			}

			ctrl.deleteImg = function() {
				ctrl.venue.general.image = '';
			}

			ctrl.save = function() {
				$ionicLoading.show({
					template: 'updating venue'
				});
				ctrl.venue
					.$save()
					.then(function(){
						$ionicLoading.hide();
					});
			}

			ctrl.goTo = function (route) {
				$state.go(route, {
					idEvent: $stateParams.idEvent,
					idVenue: $stateParams.idVenue
				});
			}
		}
	])
