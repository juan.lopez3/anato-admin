angular
	.module('venues')

	.controller('dirAddCtrl', [
		'$state', '$stateParams', 'firebaseURL', '$firebaseArray', '$firebaseObject', '$ionicPopup', '$ionicLoading',
		function ($state, $stateParams, firebaseURL, $firebaseArray, $firebaseObject, $ionicPopup, $ionicLoading) {

			var addCtrl = this;

			/*
				Firebase
				ref to directions array
			*/
			var path = firebaseURL + 'venues/' + $stateParams.idVenue +'/directions';
			var sync = new Firebase(path);

			//scope array (three way data binding)
			addCtrl.directions = $firebaseArray(sync);

			/*
				Sending data:
				1. 	send data to directions array

			*/

			//to manage data
			addCtrl.direction = {};

			addCtrl.add = function() {
				//save data in directions array
		        addCtrl.directions.$add({
	        		title: addCtrl.direction.title,
					content: addCtrl.direction.content
		        })
		        .then(function(ref) {
		        	addCtrl.direction = {};
		        	//conference key
		        	$ionicPopup.alert({
 						title: 'direction Added',
 						template: 'you can close this window'
					});
		        });
			}
		}
	])
