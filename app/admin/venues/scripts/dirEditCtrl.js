angular
	.module('venues')

	.controller('dirEditCtrl', [
		'$state','$stateParams', 'firebaseURL', '$firebaseObject', '$firebaseArray', '$ionicLoading',
		function ($state,$stateParams, firebaseURL, $firebaseObject, $firebaseArray, $ionicLoading) {

			var ctrl = this;

			//show loading while data it's retrieve
			$ionicLoading.show({
				template: 'retrieving direction data'
			});

			//path to selectec direction
			var path = firebaseURL + 'venues/' + $stateParams.idVenue + '/directions/' + $stateParams.idDir;
			//sync to firebase event's schedule
			var sync = new Firebase(path);
			//three way data binding
			ctrl.direction = $firebaseObject(sync);

			//wait for data load
			ctrl.direction
				.$loaded()
				.then(function(data){
					$ionicLoading.hide();
				});

			ctrl.save = function() {
				$ionicLoading.show({
					template: 'updating direction'
				});
				ctrl.direction
					.$save()
					.then(function(){
						$ionicLoading.hide();
					});
			}
		}
	])
