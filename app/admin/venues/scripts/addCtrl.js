angular
	.module('venues')

	.controller('venueAddCtrl', [
  '$state', '$stateParams', 'firebaseURL', '$firebaseArray', '$firebaseObject', '$ionicPopup', '$ionicLoading', 'Upload', 'SERVER_IMAGES',
  function ($state, $stateParams, firebaseURL, $firebaseArray, $firebaseObject, $ionicPopup, $ionicLoading, Upload, SERVER_IMAGES) {

			var addCtrl = this;

			/*
				Firebase
				ref to venues array
			*/
			var path = firebaseURL + 'venues';
			var sync = new Firebase(path);

			//scope array (three way data binding)
			addCtrl.venues = $firebaseArray(sync);

			/*
				Sending data:
				1. 	send data to venues array

			*/

			//to manage data
			addCtrl.venue = {};

			addCtrl.add = function() {
				//save data in venues array
		        addCtrl.venues.$add({
		        	general: {
		        		title: addCtrl.venue.title,
						shortDescription: addCtrl.venue.shortDescription,
						image:  addCtrl.venue.image
		        	},
		        	event: $stateParams.idEvent
		        })
		        .then(function(ref) {
		        	//conference key
		        	var key = ref.key();
		        	//save conferece ref in event
		        	console.log('saved');
		        	addCtrl.addEventRef(key);
		        });
			}

			addCtrl.image = function(files){
            	if (!files || files[0] == undefined){ return; }

            	$ionicLoading.show({ template: 'Uploading Image ...' });

	            var file = files[0];
	            Upload.upload({
                url: SERVER_IMAGES,
	                file: file
	            }).progress(function (evt) { })
	            .success(function (data, status, headers, config) {
					addCtrl.venue.image = data;
					$ionicLoading.hide();

	            }).error(function (data, status, headers, config) {
	                console.log('Error status: ' + status);
	            });
			}

			addCtrl.deleteImg = function() {
				ctrl.venue.image = '';
			}

			/*
				Firebase
				ref to event object
			*/

			addCtrl.addEventRef = function (venueKey) {
				var eventPath = firebaseURL + 'events/' + $stateParams.idEvent + '/venue';
				var eventSync = new Firebase(eventPath);

				var onComplete = function(error) {
					if (error) { console.log(error) }
					else {
						//clear form
						addCtrl.venue = {};
						//popup success
						$ionicPopup.alert({
     						title: 'Venue Added',
     						template: 'you can close this window'
   						});
					}
				}

				eventSync
					.set(venueKey, onComplete);
			}
		}
	])
