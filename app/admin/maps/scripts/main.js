angular.module('maps', [])

.controller('mapsCtrl', function($scope, $stateParams, firebaseURL){

  var firebase = new Firebase(firebaseURL).child("events").child($stateParams.idEvent).child("map");

  firebase.on("value", function(response){
    $scope.map = response.val();
  });

  $scope.save = function () {
    firebase.set($scope.map);
  }

  /* MAPA */

	L.mapbox.accessToken = 'pk.eyJ1IjoibW9jaW9uIiwiYSI6ImQ0MzA3MTczY2MzNzkyNDU5MGIzNzFkMGMwM2FiNDA2In0.eYjENv5PGQ1A0wwLggtB5w';

	var map = L.mapbox.map('mapid', 'mocion.n69km84f')
	.setView( [$stateParams.lat, $stateParams.long], $stateParams.zoom ); // [latitud, longitud], zoom 19max
	// London, [51.512, -0.176], 17
	// Newport, [51.602, 	], 19
	// Edinburgh,  [55.946, -3.206], 16
	// Birmingham, [52.47, -1.715], 11

});
