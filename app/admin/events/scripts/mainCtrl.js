var eventMain = angular.module('events');

//service for edit events
eventMain.factory('eventService', [
	'firebaseURL', '$firebaseObject',
	function (firebaseURL, $firebaseObject) {
		var path = firebaseURL + 'events';
		var Event = $firebaseObject.$extend({
			//metodos adicionales para este evento
		});

		return function (eventId) {
			var sync = new Firebase(path).child(eventId);
			return new Event(sync);
		}
	}
]);

eventMain.controller('adminEventCtrl', [
	'firebaseURL', '$firebaseArray', '$ionicPopup', '$ionicLoading', '$scope', '$state', '$stateParams',
	function (firebaseURL, $firebaseArray, $ionicPopup, $ionicLoading, $scope, $state, $stateParams) {
		var scope = this;

    //Firebase's variables
		var firebase = new Firebase(firebaseURL);
    var eventTypes = firebase.child('eventTypes').child($stateParams.idType);
    var events = firebase.child('events');

    //Type event
    scope.typeEvent = $stateParams.idType;

    $ionicLoading.show({ template: 'Retrieving data...' });
    events.on("value", function(data){
      scope.events = data.val();
    });

    eventTypes.on("value", function(data){
      scope.type = data.val();
      $ionicLoading.hide();
    });

		scope.new = function() {
      $ionicPopup.prompt({
        title: "TYPE EDITION NAME"
      }).then(function(response){
        if(!response)
          return;

        $ionicLoading.show({ template: 'Creating Event' });

        var key = events.push({name: response, typeEvent: $stateParams.idType}, function(error){
          $ionicLoading.hide();
          if(error) console.log('Error:', error);
        }).key();

        scope.type.events[key] = false;
        eventTypes.set(scope.type);

      });
		}

		scope.deleteEvent = function (key) {
			$ionicPopup.confirm({
        title: "Do you want to remove this event?"
			}).then(function(res) {
        if(!res)
          return;

        events.child(key).remove();
        eventTypes.child("events").child(key).remove();
			});
		};

    scope.changePrivacy = function(key, value){
      if(value)
        events.child(key).child("private").set(false);
      else
        events.child(key).child("private").set(true);
    }

    scope.changeActive = function(key, value){
      if(value){
        eventTypes.child('events').child('active').set(false);
        events.child("order").once("value", function(editions){
          events.child("order").child(editions.val().indexOf(key)).remove();
        });
      } else {
        eventTypes.child('events').child('active').set(key);
        events.child("order").once("value", function(editions){
          var array = editions.val();
          array.push(key);
          events.child("order").set(array);
        });
      }
    }

    scope.changePublish = function(key, value){
      if(value)
        eventTypes.child('events').child(key).set(false);
      else
        eventTypes.child('events').child(key).set(true);
    }

}]);
