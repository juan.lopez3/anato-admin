var eventEdit = angular.module('events');

eventEdit.controller('adminEventEditCtrl', [
  'eventService','$stateParams', '$ionicLoading', '$ionicPopup', '$state', '$firebaseObject', '$firebaseArray', 'firebaseURL', 'Upload', '$scope', 'SERVER_FILES', 'SERVER_IMAGES',
  function (eventService, $stateParams, $ionicLoading, $ionicPopup, $state, $firebaseObject, $firebaseArray, firebaseURL, Upload, $scope, SERVER_FILES, SERVER_IMAGES) {

		var eventCtrl = this;

		//show loading while data it's retrieve
		$ionicLoading.show();

		/*
				Firebase
				ref to current event
			*/

		var firebase = new Firebase(firebaseURL);
		var eventPath = firebaseURL + 'events/' + $stateParams.idEvent;
		var eventSync = new Firebase(eventPath);

		//scope array (three way data binding)
		eventCtrl.Event = $firebaseObject(eventSync);

		//Documents instance
		eventCtrl.documents = [];

		//on data loaded hide loading window - promise
		eventCtrl.Event.$loaded().then(function(response) {

			timeToDate();

			//cuando carga el evento traer modulos del evento
			firebase.child("eventTypes").on("value", function(data){
				eventCtrl.typeEvents = data.val();
				eventCtrl.modulesAvalibles = eventCtrl.typeEvents[$stateParams.idType].modules || null;
			});

			//traer documentos
			angular.forEach(response.documents, function(id) {
				eventCtrl.documents.push( $firebaseObject( new Firebase(firebaseURL+'documents/'+id) ) );
			});

			$ionicLoading.hide();

		});

		eventCtrl.change = function () {
			eventCtrl.modulesAvalibles = eventCtrl.typeEvents[eventCtrl.Event.typeEvent].modules;
		};

		eventCtrl.saveEvent = function() {

			//show loader while data it's being send
			$ionicLoading.show({
				template: 'Updating event ...'
			});

			if(eventCtrl.Event.typeEvent)
				firebase.child("eventTypes").child(eventCtrl.Event.typeEvent).child("events").child($stateParams.idEvent).set(true);

			timeToTimestamp();
			eventCtrl.Event.$save().then(function(ref) {
				//hide loader
				$ionicLoading.hide();
				//date to string
				timeToDate();
				//show success mesage
				$ionicPopup.alert({
					title: 'Success',
					template: 'Event successfully updated'
				});
			}, function(error) {
				//hide loader
				$ionicLoading.hide();
				//show error alert
				$ionicPopup.alert({
					title: 'Error',
					template: error
				});
			});
		};

		eventCtrl.goTo = function(route) {
			$state.go(route, {idEvent: $stateParams.idEvent});
		};

		eventCtrl.goToVenue = function() {
			$state.go('admin.venueEdit', {
				idEvent: $stateParams.idEvent,
				idVenue: eventCtrl.Event.venue
			});
		};

		eventCtrl.openMap = function() {
			$state.go('admin.map', {
				idEvent: $stateParams.idEvent,
				//map params
				lat: eventCtrl.Event.map.lat,
				long: eventCtrl.Event.map.long,
				zoom: eventCtrl.Event.map.zoom
			});
		}

		/*
			UPLOAD FILES
		------------------------------------------------------------------ */
		eventCtrl.uploadfiles = function(files){
			if (!files || files[0] === undefined){ return; };

			eventCtrl.Event.documents = eventCtrl.Event.documents || [];

			angular.forEach(files, function(file) {
				Upload.upload({
          url: SERVER_FILES,
					file: file,
					fields: { operation: 'insert' }
				})
					.progress(function (evt) {
					$ionicLoading.show({ template: 'Loading... ' + parseInt(100 * evt.loaded / evt.total) + "%" });
				})
					.success(function (data, status, headers, config) {
					if (data === 'pdf') {
						$ionicPopup.alert({ title: '<b>Error</b>', template: 'File(s) must be PDF or PPT.' });
						$ionicLoading.hide();
						return;

					} else if (data === 'server') {
						$ionicPopup.alert({ title: 'Error', template: 'There was a problem loading files, try again.' });
						$ionicLoading.hide();
						return;
					}

					$ionicLoading.hide();
					$scope.document = {};

					$ionicPopup.show({
						template: '<input type="text" ng-model="document.name">',
						title: 'Set the filename',
						scope: $scope,
						buttons:[
							{
								text: 'Accept',
								type: 'button-positive',
								onTap: function(e){
									return;
								}
							}
						]
					}).then(function(response) {
						$ionicLoading.show({ template: 'Uploading... ' });
						var name = ($scope.document.name === undefined) ? 'Untitled' : $scope.document.name;
						var documents = $firebaseArray(new Firebase(firebaseURL + "documents"));
						documents.$loaded();
						documents.$add({eventId: $stateParams.idEvent, url: data, name: name}).then(function(response){
							eventCtrl.Event.documents.push(response.key());
							/*timeToTimestamp();
							eventCtrl.Event.$save().then(reloadDocs());*/
							$ionicLoading.hide();
						});

					});

				});

			});
		}

		/*
			UPLOAD IMAGES
		------------------------------------------------------------------ */
		eventCtrl.uploadImages = function(files, type){
			if (!files || files[0] == undefined){ return; }
			$ionicLoading.show({ template: 'Loading...' });
			var file = files[0];
			Upload.upload({
        url: SERVER_IMAGES,
				file: file
			}).success(function (data, status, headers, config) {
				eventCtrl.Event[type] = data;
				/*timeToTimestamp();
				eventCtrl.Event.$save().then(timeToDate());*/
				$ionicLoading.hide();
      }).error(function (data, status, headers, config) {
        console.log('Error: ' + data);
        $ionicLoading.hide();
			});

		}

		/*
			EDITAR DOCUMENTOS
		------------------------------------------------------------------ */
		eventCtrl.docEdit = function(document){
			$scope.document = {name: document.name};
			$ionicPopup.show({
				template: '<input type="text" ng-model="document.name">',
				title: 'Set the filename',
				scope: $scope,
				buttons:[
					{
						text: 'Accept',
						type: 'button-positive',
						onTap: function(e){
							return;
						}
					}
				]
			}).then(function(response) {
				document.name = ($scope.document.name === undefined) ? 'Untitled' : $scope.document.name;
				document.$save().then(reloadDocs());
			});
		};

		/*
			BORRAR DOCUMENTOS
		------------------------------------------------------------------ */
		eventCtrl.docDelete = function(doc){
			$ionicPopup.confirm({
				title: 'Alert',
				template: 'Are you sure you want delete this file?'
			}).then(function(response){
				if( !response ){ return; }
				$firebaseObject( new Firebase(firebaseURL+'documents/'+doc.$id) ).$remove();
				eventCtrl.Event.documents.splice(eventCtrl.Event.documents.indexOf(doc.$id), 1);
				eventCtrl.Event.$save().then(reloadDocs());
			});
		};

		/*
			ACTUALIZAR DOCUMENTOS
		------------------------------------------------------------------ */
		function reloadDocs() {
			eventCtrl.documents = [];
			angular.forEach(eventCtrl.Event.documents, function(id) {
				eventCtrl.documents.push( $firebaseObject( new Firebase(firebaseURL+'documents/'+id) ) );
			});
		}

		/*
			DATE TO TIMESTAMP
		------------------------------------------------------------------ */
		function timeToTimestamp() {
			eventCtrl.Event.dateFrom = eventCtrl.Event.dateFrom.getTime();
			eventCtrl.Event.dateTo = eventCtrl.Event.dateTo.getTime();
		}

		/*
			TIMESTAMP TO DATE
		------------------------------------------------------------------ */
		function timeToDate() {
			eventCtrl.Event.dateFrom = new Date(eventCtrl.Event.dateFrom);
			eventCtrl.Event.dateTo = new Date(eventCtrl.Event.dateTo);
		}

		/*
			DATEPICKER FROM
		----------------------------------------------------------------------- */
		$scope.datepickerFrom = {
			titleLabel: 'Event starts at:',  //Optional
			todayLabel: 'Today',  //Optional
			closeLabel: 'Close',  //Optional
			setLabel: 'Set',  //Optional
			setButtonType : 'button-assertive',  //Optional
			todayButtonType : 'button-assertive',  //Optional
			closeButtonType : 'button-assertive',  //Optional
			inputDate: new Date(),    //Optional
			templateType: 'popup', //Optional
			showTodayButton: 'true', //Optional
			modalHeaderColor: 'bar-positive', //Optional
			modalFooterColor: 'bar-positive', //Optional
			callback: function (val) {    //Mandatory
				datePickerFromCallback(val);
			}
		};

		/* callback - DATEPICKER FROM */
		function datePickerFromCallback(data) {
			if (data === undefined) return console.log('nothing to send');
			eventCtrl.Event.dateFrom = data;
		}

		/*
			DATEPICKER TO
		----------------------------------------------------------------------- */
		$scope.datepickerTo = {
			titleLabel: 'Event ends at:',  //Optional
			todayLabel: 'Today',  //Optional
			closeLabel: 'Close',  //Optional
			setLabel: 'Set',  //Optional
			setButtonType : 'button-assertive',  //Optional
			todayButtonType : 'button-assertive',  //Optional
			closeButtonType : 'button-assertive',  //Optional
			inputDate: new Date(),    //Optional
			templateType: 'popup', //Optional
			showTodayButton: 'true', //Optional
			modalHeaderColor: 'bar-positive', //Optional
			modalFooterColor: 'bar-positive', //Optional
			callback: function (val) {    //Mandatory
				datePickerToCallback(val);
			}
		};

		/* callback - DATEPICKER TO */
		function datePickerToCallback(data) {
			if (data === undefined) return console.log('nothing to send');
			eventCtrl.Event.dateTo = data;
		}

	}//end main fn
]);
