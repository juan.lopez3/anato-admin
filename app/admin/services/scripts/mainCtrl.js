angular
	.module('services')

	.controller('servicesCtrl',[
	'$state', '$stateParams', '$firebaseArray', '$firebaseObject', 'firebaseURL', '$ionicLoading', 'Upload', 'SERVER_IMAGES',
	function ($state, $stateParams, $firebaseArray, $firebaseObject, firebaseURL, $ionicLoading, Upload, SERVER_IMAGES) {
		var ctrl = this;

		$ionicLoading.show();
		/*
			TRAER ARRAY DE SERVICIOS,
			Y RUTA AL PLANO GENERAL DE SERVICIOS
		------------------------------------------------------------------ */
		//path to event's services
		var pathList = firebaseURL + 'services/' + $stateParams.idEvent + '/list';
		var pathFloorplan = firebaseURL + 'services/' + $stateParams.idEvent + '/floorplan';
		//data binding
		ctrl.services = $firebaseArray(new Firebase(pathList));
		ctrl.floorplan = $firebaseObject(new Firebase(pathFloorplan));
		//check object has been loaded
		ctrl.services.$loaded().then(function(data){
			ctrl.floorplan.$loaded().then(function(){
				$ionicLoading.hide();
			});
		});
		/*
			GUARDAR SERVICIO MODIFICADO
		------------------------------------------------------------------ */
		ctrl.save = function(index) {
			$ionicLoading.show();
			ctrl.services.$save(index).then(function(){
				ctrl.edit = '';
				$ionicLoading.hide();
			},function(error){
				console.log(error);
			});
		};
		/*
			AGREGAR NUEVO SERVICIO AL LISTADO
		------------------------------------------------------------------ */
		ctrl.data = {};//object to store temp data
		ctrl.crear = function () {
			$ionicLoading.show();
			ctrl.services.$add(ctrl.data).then(function(){
				//si se enviaron los datos limpiamos el formulario
				ctrl.data = {};
				$ionicLoading.hide();
			}, function(error){
				$ionicLoading.hide();
				console.log(error);
			});
		};
		/*
			ELIMINAR SERVICIO DEL LISTADO
		------------------------------------------------------------------ */
		ctrl.remove = function(item) {
			$ionicLoading.show();
			ctrl.services.$remove(item).then(function(){
				$ionicLoading.hide();
			});
		};

		/*
			FLOORPLAN: REMOVE
		------------------------------------------------------------------ */
		ctrl.deleteImg = function() {
			ctrl.floorplan.img = null;
		};
		/*
			FLOORPLAN: SAVE
		------------------------------------------------------------------ */
		ctrl.saveImg = function() {
			$ionicLoading.show();
			ctrl.floorplan.$save().then(function(){
				$ionicLoading.hide();
			});
		};
		/*
			FLOORPLAN: UPLOAD
		------------------------------------------------------------------ */
		ctrl.uploadImages = function(files, type){
			if (!files || files[0] == undefined){ return; }
			$ionicLoading.show();
			var file = files[0];
			Upload.upload({
				url: SERVER_IMAGES,
				file: file
			}).success(function (data, status, headers, config) {
				ctrl.floorplan.img = data;
				$ionicLoading.hide();
			}).error(function (data, status, headers, config) {
				console.log('Error: ' + data);
				$ionicLoading.hide();
			});
		};

	}
])
