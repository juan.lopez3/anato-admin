angular
	.module('speakers', [])

	.controller('speakerMainCtrl', [
		'$state', '$stateParams', 'firebaseURL', '$firebaseArray', '$ionicLoading',
		function ($state, $stateParams, firebaseURL, $firebaseArray, $ionicLoading) {
			// body...
			var ctrl = this;

			//path to speakers
			var speakerPath = firebaseURL + 'speakers';
			//sync to firebase event's speaker
			var speakerSync = new Firebase(speakerPath);
			ctrl.speakers = $firebaseArray(speakerSync);

			//sync to firebase event's programme
			var programmeSync = new Firebase(firebaseURL + 'programme');

			ctrl.remove = function (index, id) {
				ctrl.speakers.$remove(ctrl.speakers[index]);
				//find programmes with this user
				ctrl.programmeFilter = programmeSync.orderByChild('speakerId').equalTo(id);
				ctrl.programmeSelected = $firebaseArray(ctrl.programmeFilter);

				ctrl.programmeSelected.$loaded().then(function(dataP){
					angular.forEach(dataP, function(key, value) {
						var item = ctrl.programmeSelected.$getRecord(key.$id);
						delete item.speakerId;
						ctrl.programmeSelected.$save(item);
					});
				});
			}
		}
	])
