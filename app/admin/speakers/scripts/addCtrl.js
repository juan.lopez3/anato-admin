angular
	.module('speakers')

	.controller('speakerAddCtrl', [
  '$stateParams', 'firebaseURL', '$firebaseArray', '$firebaseObject', '$ionicPopup', '$ionicLoading', 'Upload', '$scope', 'SERVER_IMAGES',
  function ($stateParams, firebaseURL, $firebaseArray, $firebaseObject, $ionicPopup, $ionicLoading, Upload, $scope, SERVER_IMAGES) {

			// body...
			var ctrl = this;

			console.log($stateParams);

			//to manage data
			ctrl.formData = {};

			/*
				Firebase
				ref to speaker array
			*/
			var path = firebaseURL + 'speakers';
			var sync = new Firebase(path);

			//scope array (three way data binding)
			ctrl.speakers = $firebaseArray(sync);

			/*

				list events
				ctrl.eventsRef = new Firebase(path = firebaseURL + 'events');
			//scope array (three way data binding)
			ctrl.events = $firebaseArray(ctrl.eventsRef);
			*/

			/*
				Sending data:
				1. 	send data to speaker array

			*/
			ctrl.formData = {};
			ctrl.formData.general = {};

			ctrl.add = function() {
				ctrl.formData[$stateParams.idEvent] = true;
				//save data in speaker array
		        ctrl.speakers
		        	.$add(ctrl.formData)
		        	.then(function(ref) {
		        		var key = ref.key();//get key
		        		ctrl.formData = {};//clear formdata
						//tell user the speaker was added
						$ionicPopup.alert({
     						title: 'Speaker Added',
     						template: 'you can close this window'
   						});
		        	});
			};

			ctrl.image = function(files){
            	if (!files || files[0] == undefined){ return; }

            	$ionicLoading.show({ template: 'Loading...' });

	            var file = files[0];
	            Upload.upload({
                  url: SERVER_IMAGES,
	                file: file
	            }).progress(function (evt) { })
	            .success(function (data, status, headers, config) {
					ctrl.formData.general.photo = data;
					$ionicLoading.hide();

	            }).error(function (data, status, headers, config) {
	                console.log('Error status: ' + status);
	            });
			}

			ctrl.deleteImg = function() {
				delete ctrl.formData.general.photo;
			};

		}
	])
