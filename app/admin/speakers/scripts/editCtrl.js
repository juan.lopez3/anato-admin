angular
	.module('speakers')

	.controller('speakerEditCtrl', [
  '$stateParams', '$ionicPopup', '$ionicLoading', '$firebaseObject', '$firebaseArray', 'firebaseURL', 'Upload', '$scope', 'SERVER_FILES', 'SERVER_IMAGES',
  function ($stateParams, $ionicPopup, $ionicLoading, $firebaseObject, $firebaseArray, firebaseURL, Upload, $scope, SERVER_FILES, SERVER_IMAGES) {

		var ctrl = this;

		//show loading while data it's retrieve
		$ionicLoading.show({
			template: 'retrieving speaker data'
		});

		//path to selectec programme
		var path = firebaseURL + '/speakers/' +$stateParams.idSpeaker;
		//sync to firebase event's schedule
		var sync = new Firebase(path);
		//three way data binding
		ctrl.speaker = $firebaseObject(sync);

		ctrl.documents = [];

		//wait for data load
		ctrl.speaker
			.$loaded()
			.then(function(data){
			ctrl.documents = ctrl.documents || [];

			angular.forEach(data.documents, function(id) {
				ctrl.documents.push( $firebaseObject( new Firebase(firebaseURL+'documents/'+id) ) );
			});

			$ionicLoading.hide();
		});

		ctrl.saveSpeaker = function() {
			$ionicLoading.show({
				template: 'saving speaker data'
			});

			ctrl.speaker
				.$save()
				.then(function(){
				$ionicLoading.hide();
			},
					  function(error){
				console.log(error);
			});
		}

		ctrl.image = function(files){
			if (!files || files[0] == undefined){ return; }

			$ionicLoading.show({ template: 'Loading...' });

			var file = files[0];
			Upload.upload({
        url: SERVER_IMAGES,
				file: file
			}).progress(function (evt) { })
				.success(function (data, status, headers, config) {
				ctrl.speaker.general.photo = data;
				$ionicLoading.hide();

			}).error(function (data, status, headers, config) {
				console.log('Error status: ' + status);
			});

		}

		ctrl.deleteImg = function () {
			delete ctrl.speaker.general.photo;
		};

		ctrl.uploadfiles = function(files){
			if (!files || files[0] === undefined){ return; };

			ctrl.speaker.documents = ctrl.speaker.documents || [];

			angular.forEach(files, function(file) {
				Upload.upload({
          url: SERVER_FILES,
					file: file,
					fields: { operation: 'insert' }
				})
					.progress(function (evt) {
					$ionicLoading.show({ template: 'Loading... ' + parseInt(100 * evt.loaded / evt.total) });
				})
					.success(function (data, status, headers, config) {
					if (data === 'pdf') {
						$ionicPopup.alert({ title: '<b>Error</b>', template: 'File(s) must be PDF or PPT.' });
						$ionicLoading.hide();
						return;

					} else if (data === 'server') {
						$ionicPopup.alert({ title: 'Error', template: 'There was a problem loading files, try again.' });
						$ionicLoading.hide();
						return;
					}

					$ionicLoading.hide();

					$scope.document = {};

					$ionicPopup.show({
						template: '<input type="text" ng-model="document.name">',
						title: 'Set the filename',
						scope: $scope,
						buttons:[
							{
								text: 'Accept',
								type: 'button-positive',
								onTap: function(e){
									return;
								}
							}
						]
					})
						.then(function(response) {
						var name = ($scope.document.name === undefined) ? 'Untitled' : $scope.document.name;

						var documents = $firebaseArray(new Firebase(firebaseURL + "documents"));
						documents.$loaded();
						documents.$add({
							url: data,
							name: name
						}).then(function(response){
							ctrl.speaker.documents.push(response.key());
							ctrl.speaker.$save().then(reloadDocs());
							$ionicLoading.hide();
						});
					});

				});

			});
		}

		ctrl.docEdit = function(document){
			$scope.document = {name: document.name};

			$ionicPopup.show({
				template: '<input type="text" ng-model="document.name">',
				title: 'Set the filename',
				scope: $scope,
				buttons:[
					{
						text: 'Accept',
						type: 'button-positive',
						onTap: function(e){
							return;
						}
					}
				]
			})
				.then(function(response) {
				document.name = ($scope.document.name === undefined) ? 'Untitled' : $scope.document.name;
				document.$save().then(reloadDocs());

			});
		}

		ctrl.docDelete = function(document){
			$ionicPopup.confirm({
				title: 'Alert',
				template: 'Are you sure you want delete this file?'
			})
				.then(function(response){
				if( !response ){ return; }

				$firebaseObject( new Firebase(firebaseURL+'documents/'+document.$id) ).$remove();

				ctrl.speaker.documents.splice(ctrl.speaker.documents.indexOf(document.$id), 1);
				ctrl.speaker.$save().then(reloadDocs());

			});
		}

		function reloadDocs() {
			ctrl.documents = [];
			angular.forEach(ctrl.speaker.documents, function(id) {
				ctrl.documents.push( $firebaseObject( new Firebase(firebaseURL+'documents/'+id) ) );
			});
		}

	}
])
