angular.module("polls", [])

.controller("pollsCtrl", function($state, $stateParams, firebaseURL, $ionicPopup){
  var scope = this;

  var firebaseEvent = new Firebase(firebaseURL).child("events").child($stateParams.idEvent).child("polls");
  var firebasePolls = new Firebase(firebaseURL).child("polls");

  scope.polls = {};

  firebaseEvent.on("value", function(eventData){
    scope.reference = eventData.val();
    firebasePolls.on("value", function(pollsData){
      if(!pollsData.val())
        return;

      angular.forEach(eventData.val(), function(uid){
        if(uid == 0){return 0}
        scope.polls[uid] = pollsData.val()[uid];
      });
      scope.polls["active"] = eventData.val().active || false;
      console.dir(scope.polls);
    });
  });

  scope.addPoll = function(){
    $ionicPopup.prompt({
      title: 'Insert poll title'
    }).then(function(title){
      if(title){
        var key = firebasePolls.push({title: title, done: false}).key();
        firebaseEvent.push(key);
        firebaseEvent.child('active').set(scope.polls.active);
        $state.go("admin.editPoll", {idEvent: $stateParams.idEvent, idPoll: key});
      }
    });
  }

  scope.edit = function(key){
    $state.go("admin.editPoll", {idEvent: $stateParams.idEvent, idPoll: key});
  }

  scope.publish = function(key){
    /*scope.polls.active = key;
    scope.polls[key].done = true;*/
    firebasePolls.child(key).child("done").set(true);
    firebaseEvent.child("active").set(key);
  }

  scope.unpublish = function(key){
    /*scope.polls.active = false;*/
    firebaseEvent.child("active").set(false);
  }

  scope.remove = function(uid){
    $ionicPopup.confirm({
      title: "This will remove all the event and statistics, irreversibly."
    }).then(function(response){
      if(!response)
        return;

      delete scope.polls[uid];
      angular.forEach(scope.reference, function(poll, index){
        if(uid == poll){
          firebaseEvent.child(index).remove();
          firebasePolls.child(uid).remove();
        }
      });
    });
  }

});
