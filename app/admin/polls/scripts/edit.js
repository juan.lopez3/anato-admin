angular.module("polls")

.controller("pollEditCtrl", function($state, $stateParams, $scope, firebaseURL, $ionicPopup){

  var firebase = new Firebase(firebaseURL).child("polls").child($stateParams.idPoll);

  firebase.on("value", function(data){
    $scope.poll = data.val();
    if(!$scope.poll.questions){
      firebase.child("questions").push({title: "", description: "", total: 0});
    }
  });

  $scope.addQuestion = function(){
    firebase.child("questions").push({title: "", description: "", total: 0});
  }

  $scope.addAnswers = function(key){
    $ionicPopup.prompt({
      title: 'Type text answer'
    }).then(function(response){
      if(response){
        if($scope.poll.questions[key].answers){
          $scope.poll.questions[key].answers.push(response);
          $scope.poll.questions[key].votes.push(0);
        } else {
          $scope.poll.questions[key].answers = [response];
          $scope.poll.questions[key].votes = [0];
        }
      }
    });
  }

  $scope.editAnswer = function(key, id){
    $ionicPopup.prompt({
      title: 'Type text to change.'
    }).then(function(response){
      if(response)
        $scope.poll.questions[key].answers[id] = response;
    });
  }

  $scope.getTotal = function(key){
    var total = 0;
    angular.forEach($scope.poll.questions[key].votes, function(value, key){
      total += value;
    });
    return total;
  }

  $scope.deleteAnswer = function(key, id){
    $scope.poll.questions[key].votes.splice(id, 1);
    $scope.poll.questions[key].answers.splice(id, 1);
  }

  $scope.savePoll = function(){
    firebase.update($scope.poll);
    $ionicPopup.alert({title: "Information saved."});
  }

});
