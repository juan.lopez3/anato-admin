angular
	.module('planos')

	.controller('planosCtrl',[
	'$state', '$stateParams', '$firebaseArray', 'firebaseURL', '$ionicLoading', 'Upload', 'SERVER_IMAGES',
	function ($state, $stateParams, $firebaseArray, firebaseURL, $ionicLoading, Upload, SERVER_IMAGES) {
		var ctrl = this;

		$ionicLoading.show({
			template: 'Loading...'
		});

		//path to event's planos
		var path = firebaseURL + 'planos/' + $stateParams.idEvent;
		//sync to firebase event's planos
		var sync = new Firebase(path);
		//data binding
		ctrl.planos = $firebaseArray(sync);

		//check object has been loaded
		ctrl.planos.$loaded().then(function(data){
			$ionicLoading.hide();
		});

		ctrl.save = function(index) {
			$ionicLoading.show({
				template: 'saving...'
			});

			ctrl.planos.$save(index).then(function(){
				ctrl.edit = '';
				$ionicLoading.hide();
			},function(error){
				console.log(error);
			});
		}

		ctrl.data = {};//object to store temp data

		ctrl.crear = function () {
			$ionicLoading.show({
				template: 'saving...'
			});
			ctrl.planos.$add(ctrl.data).then(function(){
				//si se enviaron los datos limpiamos el formulario
				ctrl.data = {};
				$ionicLoading.hide();
			}, function(error){
				$ionicLoading.hide();
				console.log(error);
			});
		};

		ctrl.remove = function(item) {
			$ionicLoading.show();
			ctrl.planos.$remove(item).then(function(){
				$ionicLoading.hide();
			});
		};

		/*
			update image
		------------------------------------------------------------------ */
		ctrl.updateImage = function(files, type, index) {
			if (!files || files[0] == undefined){ return; }
			$ionicLoading.show({ template: 'Loading...' });
			var file = files[0];
			Upload.upload({
				url: SERVER_IMAGES,
				file: file
			}).success(function (data, status, headers, config) {
				//update current item
				ctrl.planos[index].img = data;
				ctrl.planos.$save(index).then(function(){
					//save complete
					$ionicLoading.hide();
				});
			}).error(function (data, status, headers, config) {
				console.log('Error: ' + data);
				$ionicLoading.hide();
			});
		};


		/*
			UPLOAD IMAGES
		------------------------------------------------------------------ */
		ctrl.uploadImages = function(files, type){
			if (!files || files[0] == undefined){ return; }
			$ionicLoading.show({ template: 'Loading...' });
			var file = files[0];
			Upload.upload({
				url: SERVER_IMAGES,
				file: file
			}).success(function (data, status, headers, config) {
				ctrl.data.img = data;
				$ionicLoading.hide();
			}).error(function (data, status, headers, config) {
				console.log('Error: ' + data);
				$ionicLoading.hide();
			});
		}

	}
])
