angular.module("settings")

  .controller("settingsCtrl", function(firebaseURL, $ionicPopup, $ionicLoading, SERVER_USERS, Upload){
  var scope = this;

  var firebase = new Firebase(firebaseURL);

  $ionicLoading.show({ template: 'Loading...' });

  /*
    Administrators
  */
  firebase.child("administrators").on("value", function(data){
    scope.administrators = data.val();
  });

  firebase.child("assembly").on("value", function(data){
    scope.asamblea = [];
    data.forEach(function(user){
      firebase.child("users").child(user.key()).child("info").child("email").once("value", function(email){
        scope.asamblea.push({id: user.key(), email: email.val()});
      });
    });
    $ionicLoading.hide();
  })

  scope.removeAdmin = function(admin){
    $ionicPopup.confirm({ title: "Are you sure?" })
    .then(function(response){
      if(response)
        firebase.child("administrators").child(scope.administrators.indexOf(admin)).remove();
    });
  }

  scope.addAdmin = function(){
    $ionicPopup.prompt({title: "Type user's email", inputType: "email", inputPlaceholder: "example@email.com"})
    .then(function(response){
      if(response){
        scope.administrators.push(response);
        firebase.child("administrators").set(scope.administrators);
      }
    });
  }

  scope.removeUsuario = function(id){
    $ionicPopup.confirm({ title: "Está seguro?" })
      .then(function(response){
      if(response){
        firebase.child("assembly").child(id).remove();
        firebase.child("users").child(id).child("employed").remove();
      }
    });
  }

  /*
    Usuarios con permisos internos.
  */
  scope.uploadUsers = function(files){
    if (!files || files[0] == undefined)
      return;

    $ionicLoading.show({ template: 'Loading...' });

    Upload.upload({
      url: SERVER_USERS,
      file: files[0]
    }).success(function (emails) {

      console.dir(emails);

      firebase.child("users").once("value", function(datasnap){
        var data = datasnap.val();
        var index = {};

        datasnap.forEach(function(user){
          if(user.val() && user.val().info)
            index[user.val().info.email] = user.key();
        });

        angular.forEach(emails, function(email){
          if (!index[email])
            return;

          data[index[email]].employed = true;
          firebase.child('users').child(index[email]).set(data[index[email]]);
          firebase.child("assembly").once("value", function(data){
            var temp = (data.val() != null) ? data.val() : {};
            temp[index[email]] = true;
            firebase.child("assembly").set(temp);
          });
        });

        $ionicLoading.hide();
        $ionicPopup.alert({title: 'Usuarios cargados correctamente'});

      });

    }).error(function (data, status, headers, config) {
      console.log('Error!');
      $ionicLoading.hide();
    });

  }

});
