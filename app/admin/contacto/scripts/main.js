angular
	.module('contacto', [])
	.config(function($stateProvider, $urlRouterProvider) {

	$stateProvider
	/*
	  	Rates
	  --------------------------------------------------------- */
		.state('admin.contacto', {
		url: '/:idEvent/contacto',
		views: {
			'adminView': {
				templateUrl: 'app/admin/contacto/views/index.tpl.html'
			}
		}
	})
})
