angular
	.module('contacto')

	.controller('contactoCtrl',[
	'$state', '$stateParams', '$firebaseArray', '$firebaseObject', 'firebaseURL', '$ionicLoading', 'Upload', 'SERVER_IMAGES',
	function ($state, $stateParams, $firebaseArray, $firebaseObject, firebaseURL, $ionicLoading, Upload, SERVER_IMAGES) {
		var ctrl = this;

		$ionicLoading.show();
		/*
			TRAER CONTACTO
		------------------------------------------------------------------ */
		//path to event's contacto
		var pathList = firebaseURL + 'contacto/' + $stateParams.idEvent;
		//data binding
		ctrl.contacto = $firebaseObject(new Firebase(pathList));
		ctrl.telefonos = $firebaseArray(new Firebase(pathList).child('telefonos'));
		//check object has been loaded
		ctrl.contacto.$loaded().then(function(data){
				$ionicLoading.hide();
		});
		/*
			GUARDAR CONTACTO
		------------------------------------------------------------------ */
		ctrl.saveEmail = function() {
			$ionicLoading.show();
			ctrl.contacto.$save().then(function(){
				$ionicLoading.hide();
			});
		};

		/*
			GUARDAR TARIFA MODIFICADO
		------------------------------------------------------------------ */
		ctrl.save = function(index) {
			$ionicLoading.show();
			ctrl.telefonos.$save(index).then(function(){
				ctrl.edit = '';
				$ionicLoading.hide();
			},function(error){
				console.log(error);
			});
		};
		/*
			AGREGAR NUEVO TARIFA AL LISTADO
		------------------------------------------------------------------ */
		ctrl.data = {};//object to store temp data
		ctrl.crear = function () {
			$ionicLoading.show();
			ctrl.telefonos.$add(ctrl.data).then(function(){
				//si se enviaron los datos limpiamos el formulario
				ctrl.data = {};
				$ionicLoading.hide();
			}, function(error){
				$ionicLoading.hide();
				console.log(error);
			});
		};
		/*
			ELIMINAR TARIFA DEL LISTADO
		------------------------------------------------------------------ */
		ctrl.remove = function(item) {
			$ionicLoading.show();
			ctrl.telefonos.$remove(item).then(function(){
				$ionicLoading.hide();
			});
		};

	}
])
