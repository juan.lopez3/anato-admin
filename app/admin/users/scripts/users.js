angular.module('users')

.controller('usersCtrl', function($state, $stateParams, firebaseURL, $scope, $ionicLoading){
	var scope = this;

	var users = new Firebase(firebaseURL).child("users");

	$ionicLoading.show();

	users.on("value", function(data){
		scope.users = data.val();
		var count = 0;
		data.forEach(function() {
		   count++;
		});
		scope.total = count;
		$ionicLoading.hide();
	});

	$scope.exportData = function(){
		var blob = new Blob([document.getElementById('table-export').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, "Users.xls");
	}

});