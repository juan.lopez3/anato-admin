angular
	.module('terms')

	.controller('termsCtrl',[
	'$state', '$stateParams', '$firebaseObject', 'firebaseURL', '$ionicLoading',
	function ($state, $stateParams, $firebaseObject, firebaseURL, $ionicLoading) {
		var ctrl = this;

		$ionicLoading.show({
			template: 'Loading...'
		});

		//path to event's terms
		var path = firebaseURL + 'terms';
		//sync to firebase event's terms
		var sync = new Firebase(path);
		//data binding
		ctrl.terms = $firebaseObject(sync);

		//check object has been loaded
		ctrl.terms.$loaded().then(function(data){
			$ionicLoading.hide();
		});

		ctrl.save = function() {
			$ionicLoading.show({
				template: 'saving terms...'
			});

			ctrl.terms.$save().then(function(){
				$ionicLoading.hide();
			},function(error){
				console.log(error);
			});
		}
	}
])
