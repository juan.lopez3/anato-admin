var faq = angular.module('FAQS');

faq.controller('FAQSEditCtrl',[
	'$stateParams', '$ionicLoading', '$firebaseObject', 'firebaseURL',
	function ($stateParams, $ionicLoading, $firebaseObject, firebaseURL) {

		var ctrl = this;

		//show loading while data it's retrieve
		$ionicLoading.show({
			template: 'retrieving FAQ data'
		});

		//path to selectec programme
		var path = firebaseURL + '/FAQS/' + $stateParams.idEvent + '/' + $stateParams.idFAQ;
		//sync to firebase event's schedule
		var sync = new Firebase(path);
		//three way data binding
		ctrl.FAQ = $firebaseObject(sync);

		//wait for data load
		ctrl.FAQ
			.$loaded()
			.then(function(data){
			$ionicLoading.hide();
		});

		ctrl.saveFAQ = function() {
			$ionicLoading.show({
				template: 'saving FAQ data'
			});

			ctrl.FAQ
				.$save()
				.then(function(){
				$ionicLoading.hide();
			},
							function(error){
				console.log(error);
			});
		}
	}
]);
