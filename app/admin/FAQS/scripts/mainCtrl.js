var faqM = angular.module('FAQS');

faqM.controller('FAQSCtrl',[
	'$scope','$state', '$stateParams', '$firebaseArray', 'firebaseURL', '$ionicLoading', '$ionicPopup',
	function ($scope,$state, $stateParams, $firebaseArray, firebaseURL, $ionicLoading, $ionicPopup) {
		var ctrl = this;

		$ionicLoading.show({
			template: '<ion-spinner icon="ripple"></ion-spinner>'
		});

		//path to event's FAQS
		var path = firebaseURL + '/FAQS/'+$stateParams.idEvent;
		//sync to firebase event's FAQS
		var sync = new Firebase(path);
		//data binding
		ctrl.FAQS = $firebaseArray(sync);

		//check object has been loaded
		ctrl.FAQS.$loaded().then(function(data){
			$ionicLoading.hide();
		});

		ctrl.edit = function(id) {
			$state.go('admin.faqsEdit',{
				idEvent: $stateParams.idEvent,
				idFAQ: id
			});
		};

		/*
    Crear FAQS - popup de confirmación
		-------------------------------------------------- */
		ctrl.newFAQS = function() {
			$scope.FAQSObj = {}/*1*/
			var newFAQSPopup = $ionicPopup.confirm({
				title: "QUESTION",
				template: '<input type="text" ng-model="FAQSObj.question">',
				scope: $scope
			});
			newFAQSPopup.then(function(res) {
				if(res) {
					ctrl.newFAQSConfirm($scope.FAQSObj);
				} else {
					console.log('You are not sure');
				}
			});
		};

		/*
    Crear FAQS  - enviar dato a firebase
  	-------------------------------------------------- */
		ctrl.newFAQSConfirm = function(data) {
			$ionicLoading.show({template: '<ion-spinner icon="ripple"></ion-spinner>'});
			ctrl.FAQS.$add(data).then(function(ref) {
				var id= ref.key();
				$state.go("admin.faqsEdit", {
					idEvent: $stateParams.idEvent,
					idFAQ: id
				});
			});
		};

		/*
    Borrar FAQ
  	-------------------------------------------------- */
		ctrl.remove = function (record) {
			var newEventPopup = $ionicPopup.confirm({
				title: "Please Confirm:",
				template: 'Do you want to remove this FAQ?',
				scope: $scope
			});
			newEventPopup.then(function(res) {
				if(res) {
					//esta linea borra el tipo de evento
					ctrl.FAQS.$remove(record);
				} else {
					console.log('You are not sure');
				}
			});
		};

	}
])
