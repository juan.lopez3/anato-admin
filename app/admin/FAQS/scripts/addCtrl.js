angular
	.module('FAQS')

	.controller('FAQSAddCtrl',[
		'firebaseURL', '$firebaseArray', '$ionicPopup', '$ionicLoading',
		function FAQSAddCtrl (firebaseURL, $firebaseArray, $ionicPopup, $ionicLoading) {
			// body...
			var addCtrl = this;

			/*
				Firebase
				ref to FAQS array
			*/
			var path = firebaseURL + 'FAQS';
			var sync = new Firebase(path);

			//scope array (three way data binding)
			addCtrl.FAQS = $firebaseArray(sync);

			/*
				Sending data:
				1. 	send data to FAQS array

			*/

			//to manage data
			addCtrl.FAQ = {};

			addCtrl.add = function() {
				//save data in FAQS array
		        addCtrl.FAQS
		        	.$add({
						question: addCtrl.FAQ.question,
						answer: addCtrl.FAQ.answer
		        	})
		        	.then(function(ref) {
		        		//clear FAQS
		        		addCtrl.FAQS = {};
		        		//say admin FAQS was created
			        	$ionicPopup.alert({
	     					title: 'FAQS Added',
	     					template: 'you can close this window'
	   					});
		        	});
			}//end add function
		}
	])

