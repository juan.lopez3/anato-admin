var exh = angular.module('exhibitor');

exh.controller('exhibitorCtrl',[
	'$scope', '$state', '$stateParams', '$firebaseArray', 'firebaseURL', '$ionicLoading', '$ionicPopup',
	function ($scope, $state, $stateParams, $firebaseArray, firebaseURL, $ionicLoading, $ionicPopup) {
		var ctrl = this;

    ctrl.idEvent = $stateParams.idEvent;

		$ionicLoading.show({template: '<ion-spinner icon="ripple"></ion-spinner>'});

		//sync to firebase event's sponsors
    var firebase = new Firebase(firebaseURL).child("exhibitors");

    firebase.on("value", function(datasnap){
      ctrl.exhibitors = datasnap.val();

      angular.forEach(ctrl.exhibitors, function(exhibitor, id){
        if(exhibitor.event != ctrl.idEvent)
          delete ctrl.exhibitors[id];
      });

      $ionicLoading.hide();
    });

		/*
    Crear exhibitor - popup de confirmación
		-------------------------------------------------- */
		ctrl.newExhibitor = function() {
			$scope.ExhibitorObj = {}/*1*/
			$ionicPopup.prompt({
				title: "COMPANY NAME",
			}).then(function(response) {
				if(response){
          $state.go("admin.exhibitorEdit", {idEvent: ctrl.idEvent, idExhibitor: firebase.push({company: response, event: ctrl.idEvent}).key()});
        }
			});
		};

		/*
    Borrar exhibitor
  	-------------------------------------------------- */
		ctrl.remove = function (id) {
			$ionicPopup.confirm({
				title: "Please Confirm:",
				template: 'Do you want to remove this exhibitor?',
			}).then(function(res) {
				if(res)
          firebase.child(id).remove();
			});
		};

	}
]);
