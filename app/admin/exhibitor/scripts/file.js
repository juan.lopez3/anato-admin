angular.module('exhibitor')

.constant("CSV_CONFIG", ["pavilion", "stand", "company", "services", "representative", "address", "pnumber", "email", "website", "city", "country", "activityArea"])

.controller("fileExhibitors", function($stateParams, Upload, $ionicPopup, $ionicLoading, firebaseURL, SERVER_EXHIBITORS, CSV_CONFIG){
  var scope = this;

  var firebase = new Firebase("https://anatoapp.firebaseio.com/").child("exhibitors");

  scope.exhibitors = [];

  scope.uploadUsers = function(files){
    if (!files || files[0] == undefined)
      return;

    $ionicLoading.show({ template: 'Subiendo archivo' });

    firebase.once("value", function(datasnap){
      datasnap.forEach(function(exhibitor){
        if(exhibitor.val().event == $stateParams.idEvent)
          firebase.child(exhibitor.key()).remove();
      });
    });

    Upload.upload({
      url: SERVER_EXHIBITORS,
      file: files[0]
    }).success(function (table) {

      $ionicLoading.hide();

      var count = 0;

      $ionicLoading.show({ template: 'Cargando expositores ' + count + "/" + table.length });

      angular.forEach(table, function(expositor){
        var info = {event: $stateParams.idEvent};
        for(var i = 0; i < CSV_CONFIG.length; i++){
          info[CSV_CONFIG[i]] = (expositor[i]) ? expositor[i] : " ";
        }

        firebase.push(info);
        scope.exhibitors.push(info);
        count++;
      });

      scope.showTable = true;

      $ionicLoading.hide();

    }).error(function (data, status, headers, config) {
      console.log('Error!');
      $ionicLoading.hide();
    });

  }

});
