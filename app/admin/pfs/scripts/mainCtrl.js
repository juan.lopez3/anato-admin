angular
	.module('pfs')

	.controller('pfsCtrl',[
		'$state', '$stateParams', '$firebaseObject', 'firebaseURL', '$ionicLoading',
		function ($state, $stateParams, $firebaseObject, firebaseURL, $ionicLoading) {
			var ctrl = this;

			$ionicLoading.show({
				template: 'Loading pfs'
			});

			//path to event's pfs
			var path = firebaseURL + '/pfs';
			//sync to firebase event's pfs
			var sync = new Firebase(path);
			//data binding
			ctrl.pfs = $firebaseObject(sync);

			//check object has been loaded
			ctrl.pfs
				.$loaded()
				.then(function(data){
					$ionicLoading.hide();
				});

			ctrl.save = function() {
				$ionicLoading.show({
					template: 'saving pfs data'
				});

				ctrl.pfs
					.$save()
					.then(function(){
						$ionicLoading.hide();
					},
					function(error){
						console.log(error);
					});
			}
		}
	])
