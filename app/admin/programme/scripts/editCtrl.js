var programme = angular.module('programme');

programme.controller('programmeEditCtrl', function ($state, $stateParams, firebaseURL, $firebaseObject, $firebaseArray, $ionicPopup, $ionicLoading, $ionicHistory, $scope, Upload, SERVER_FILES) {

    var ctrl = this;

    ctrl.tipo = $stateParams.type;

    //show loading while data it's retrieve
    $ionicLoading.show({
        template: 'retrieving programme data'
    });

    //path to selectec programme
    var path = firebaseURL + 'programme/' + $stateParams.idProgramme;
    //sync to firebase event's schedule
    var sync = new Firebase(path);
    //three way data binding
    ctrl.conference = $firebaseObject(sync);

    var server = SERVER_FILES;

    new Firebase(firebaseURL).child("events").child($stateParams.idEvent).on("value", function (response) {
        var from = new Date(response.val().dateFrom);
        var to = new Date(response.val().dateTo);

        ctrl.days = {};

        do {
            ctrl.days[(from.getMonth() + 1) + "/" + from.getDate()] = {
                value: from.getTime(),
                text: from.getDate() + "/" + (from.getMonth() + 1)
            };
            from.setDate(from.getDate() + 1);
        } while (from.getDate() <= to.getDate());

    });

    ctrl.conference.$loaded().then(function (data) {
        ctrl.documents = ctrl.documents || [];
        angular.forEach(data.documents, function (id) {
            ctrl.documents.push($firebaseObject(new Firebase(firebaseURL + 'documents/' + id)));
        });

        $ionicLoading.hide();
    });

    ctrl.saveData = function () {
        $ionicLoading.show({
            template: 'updating programme'
        });

        ctrl.conference.$save().then(function () {
            ctrl.speakersObj.$save();
            $ionicLoading.hide();
        });
    };

    ctrl.delete = function () {
        var confirmPopup = $ionicPopup.confirm({
            title: 'Remove Session',
            template: 'Are you sure you want to remove this session?'
        });
        confirmPopup.then(function (res) {
            if (res) {
                console.log('You are sure');
                ctrl.conference.$remove().then(function () {
                    $ionicHistory.goBack();
                });
            } else {
                console.log('You are not sure');
            }
        });
    };

    /*
     go to edit/create speaker
     */
    ctrl.editSpeaker = function (id) {
        $state.go('admin.speakerEdit', {
            idEvent: $stateParams.idEvent,
            idProgramme: $stateParams.idProgramme,
            idSpeaker: id
        });
    };

    ctrl.createSpeaker = function () {
        $state.go('admin.speakerAdd', {
            idEvent: $stateParams.idEvent,
            idProgramme: $stateParams.idProgramme
        });
    }

    /*
     List speakers
     */

    //path to speakers
    var speakerPath = firebaseURL + 'speakers';
    //sync to firebase event's speaker
    var speakerSync = new Firebase(speakerPath);
    ctrl.speakers = $firebaseArray(speakerSync);
    ctrl.speakersObj = $firebaseObject(speakerSync);

    /*
     Multiples speakers por session
     1. agregar a session actual
     2. delete
     */
    //1
    ctrl.addSpeaker = function () {
        //add speaker to session
        if (!ctrl.selectedSpeaker || ctrl.selectedSpeaker === '')
            return;
        ctrl.conference.speakers = ctrl.conference.speakers || {};
        ctrl.conference.speakers[ctrl.selectedSpeaker] = true;
        //save ref in speaker - events > id evento > id sesión
        //verificar si el speaker tiene eventos referenciados
        ctrl.speakersObj[ctrl.selectedSpeaker].events = ctrl.speakersObj[ctrl.selectedSpeaker].events || {};
        //verificar si el speaker tiene referencia al evento actual
        ctrl.speakersObj[ctrl.selectedSpeaker].events[$stateParams.idEvent] = ctrl.speakersObj[ctrl.selectedSpeaker].events[$stateParams.idEvent] || {};
        //crear nueva referencia
        ctrl.speakersObj[ctrl.selectedSpeaker].events[$stateParams.idEvent][$stateParams.idProgramme] = true;
    };
    //2
    ctrl.speakerRemove = function (id) {
        delete ctrl.conference.speakers[id];
        delete ctrl.speakersObj[id].events[$stateParams.idEvent][$stateParams.idProgramme];
    };


    /*
     Documents
     */

    ctrl.uploadfiles = function (files) {
        if (!files || files[0] === undefined) {
            return;
        }
        ;

        ctrl.documents = ctrl.documents || [];

        angular.forEach(files, function (file) {
            Upload.upload({
                url: server,
                file: file,
                fields: {operation: 'insert'}
            })
                .progress(function (evt) {
                    $ionicLoading.show({template: 'Loading... ' + parseInt(100 * evt.loaded / evt.total)});
                })
                .success(function (data, status, headers, config) {
                    if (data === 'pdf') {
                        $ionicPopup.alert({title: '<b>Error</b>', template: 'File(s) must be PDF or PPT.'});
                        $ionicLoading.hide();
                        return;

                    } else if (data === 'server') {
                        $ionicPopup.alert({title: 'Error', template: 'There was a problem loading files, try again.'});
                        $ionicLoading.hide();
                        return;
                    }

                    $ionicLoading.hide();

                    $scope.document = {};

                    $ionicPopup.show({
                        template: '<input type="text" ng-model="document.name">',
                        title: 'Set the filename',
                        scope: $scope,
                        buttons: [
                            {
                                text: 'Accept',
                                type: 'button-positive',
                                onTap: function (e) {
                                    return;
                                }
                            }
                        ]
                    })
                        .then(function (response) {
                            var name = ($scope.document.name === undefined) ? 'Untitled' : $scope.document.name;

                            var documents = $firebaseArray(new Firebase(firebaseURL + "documents"));
                            documents.$loaded();

                            documents
                                .$add({
                                    eventId: $stateParams.idEvent,
                                    url: data,
                                    name: name
                                })
                                .then(function (response) {
                                    ctrl.conference.documents = ctrl.conference.documents || [];
                                    ctrl.conference.documents.push(response.key());
                                    ctrl.conference.$save().then(reloadDocs());
                                    $ionicLoading.hide();
                                });
                        });

                });

        });

    }

    ctrl.docEdit = function (document) {
        $scope.document = {name: document.name};

        $ionicPopup.show({
            template: '<input type="text" ng-model="document.name">',
            title: 'Set the filename',
            scope: $scope,
            buttons: [
                {
                    text: 'Accept',
                    type: 'button-positive',
                    onTap: function (e) {
                        return;
                    }
                }
            ]
        })
            .then(function (response) {
                document.name = ($scope.document.name === undefined) ? 'Untitled' : $scope.document.name;
                document.$save().then(reloadDocs());

            });
    }

    ctrl.docDelete = function (document) {
        $ionicPopup.confirm({
            title: 'Alert',
            template: 'Are you sure you want delete this file?'
        })
            .then(function (response) {
                if (!response) {
                    return;
                }

                $firebaseObject(new Firebase(firebaseURL + 'documents/' + document.$id)).$remove();
                ctrl.conference.documents.splice(ctrl.conference.documents.indexOf(document.$id), 1);
                ctrl.conference.$save().then(reloadDocs());
            });
    };

    function reloadDocs() {
        ctrl.documents = [];
        angular.forEach(ctrl.conference.documents, function (id) {
            ctrl.documents.push($firebaseObject(new Firebase(firebaseURL + 'documents/' + id)));
        });
    }
});
