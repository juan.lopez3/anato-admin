var module = angular.module('programme', ['angular.filter']);

module.filter('reverse', function () {
  return function (items) {
    items = Object.keys(items).map(function (key) {
      return items[key]
    });
    return items.slice();
  };
});

//PROGRAMME Logic
module.controller('adminScheduleCtrl', function ($state, $stateParams, $firebaseArray, firebaseURL, $ionicLoading) {
  var ctrl = this;

  $ionicLoading.show({
    template: 'Loading programme'
  });

  //path to event's schedule
  var path = firebaseURL + '/programme';
  //sync to firebase event's schedule
  var sync = new Firebase(path);
  //query
  var syncFilter = sync.orderByChild('event').equalTo($stateParams.idEvent);
  //data binding
  ctrl.schedule = $firebaseArray(syncFilter);

  //check object has been loaded
  ctrl.schedule.$loaded().then(function (data) {
    angular.forEach(ctrl.schedule, function (item, key) {
      ctrl.schedule[key].general.origin = parseInt(ctrl.schedule[key].general.start.replace(':', ''));
      ctrl.schedule[key].general.day = new Date(item.general.day);
      ctrl.schedule[key].general.day.setHours(0);
      ctrl.schedule[key].general.day.setMinutes(0);
      ctrl.schedule[key].general.day.setSeconds(0);
    });

    $ionicLoading.hide();
  });

  ctrl.addProgramme = function (tipo) {
    $state.go('admin.ScheduleAdd', {
      idEvent: $stateParams.idEvent,
      type: tipo
    });
  };

  ctrl.editProgramme = function (id, tipo) {
    //if (type === 'Brake') return;

    $state.go('admin.ScheduleEdit', {
      idEvent: $stateParams.idEvent,
      idProgramme: id,
      type: tipo
    });
  }

});