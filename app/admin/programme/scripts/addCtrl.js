var module = angular.module('programme');

//creamos un servicio para compartir los datos del fomulario
//en diferentes controladores
module.service('programmeForm', function ProgrammeForm() {
    var programmeForm = this;
    programmeForm = {};
});

module.controller('scheduleAddCtrl', function ($state, $stateParams, programmeForm, firebaseURL, $firebaseArray, $firebaseObject, $ionicPopup, $scope) {

    var addCtrl = this;
    addCtrl.type = $stateParams.type;

    //to manage data
    addCtrl.formData = {};

    /*
     Firebase
     ref to programme array
     */
    var path = firebaseURL + 'programme';
    var sync = new Firebase(path);

    //scope array (three way data binding)
    addCtrl.programme = $firebaseArray(sync);

    new Firebase(firebaseURL).child("events").child($stateParams.idEvent).on("value", function (data) {
        var from = new Date(data.val().dateFrom);
        var to = new Date(data.val().dateTo);

        addCtrl.days = {};

        do {
            addCtrl.days[(from.getMonth() + 1) + "/" + from.getDate()] = {
                value: from.getTime(),
                text: from.getDate() + "/" + (from.getMonth() + 1)
            };
            from.setDate(from.getDate() + 1);
        } while (from.getDate() <= to.getDate());

    });

    /*
     Sending data:
     1. 	send data to programme array

     */
    addCtrl.add = function () {
        if (addCtrl.type === 'Brake')
            addCtrl.formData.description = false;
        //save data in programme array
        var temp = new Date(addCtrl.formData.day);
        temp.setHours(addCtrl.formData.start.split(":")[0]);
        temp.setMinutes(addCtrl.formData.start.split(":")[1]);
        console.log(temp);
        addCtrl.formData.day = temp.getTime();

        addCtrl.programme.$add({
            general: {
                name: addCtrl.formData.name,
                start: addCtrl.formData.start,
                end: addCtrl.formData.end || null,
                description: addCtrl.formData.description || null,
                day: addCtrl.formData.day || null
            },
            event: $stateParams.idEvent,
            type: addCtrl.type,
            speaker: false
        }).then(function (ref) {
            //conference key
            var key = ref.key();
            //save conferece ref in event
            addCtrl.addEventRef(key);
        });
    }

    /*
     Firebase
     ref to event object
     */

    addCtrl.addEventRef = function (programmeKey) {
        var eventPath = firebaseURL + 'events/' + $stateParams.idEvent + '/programme/' + programmeKey;
        var eventSync = new Firebase(eventPath);

        var onComplete = function (error) {
            if (error) {
                console.log(error)
            } else {
                //clear form
                addCtrl.formData = {};
                //popup success
                $ionicPopup.alert({
                    title: addCtrl.type + 'Added',
                    template: 'you can close this window'
                });
            }
        }

        eventSync.set(true, onComplete);
    }

})
