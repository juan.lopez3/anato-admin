angular
	.module('acomodation')

	.controller('acomodationEditCtrl',[
		'$state','$stateParams', '$ionicLoading', '$firebaseObject', 'firebaseURL',
		function ($state, $stateParams, $ionicLoading, $firebaseObject, firebaseURL) {

			var ctrl = this;

			//show loading while data it's retrieve
			$ionicLoading.show({
				template: 'retrieving acomodation data'
			});

			//path to selectec programme
			var path = firebaseURL + '/acomodation/' + $stateParams.idAcomodation;
			//sync to firebase event's schedule
			var sync = new Firebase(path);
			//three way data binding
			ctrl.acomodation = $firebaseObject(sync);

			//wait for data load
			ctrl.acomodation
				.$loaded()
				.then(function(data){
					$ionicLoading.hide();
				});

			ctrl.save = function() {
				$ionicLoading.show({
					template: 'saving acomodation data'
				});

				ctrl.acomodation
					.$save()
					.then(function(){
						$ionicLoading.hide();
					},
					function(error){
						console.log(error);
					});
			}

			ctrl.openMap = function() {
				$state.go('admin.map', {
					idEvent: $stateParams.idEvent,
					//map params
					lat: ctrl.acomodation.lat,
					long: ctrl.acomodation.long,
					zoom: ctrl.acomodation.zoom
				});
			}
		}
	])
