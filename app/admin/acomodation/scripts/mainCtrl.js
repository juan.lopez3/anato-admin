angular
	.module('acomodation')

	.controller('acomodationCtrl',[
		'$state', '$stateParams', '$firebaseArray', 'firebaseURL', '$ionicLoading',
		function ($state, $stateParams, $firebaseArray, firebaseURL, $ionicLoading) {
			var ctrl = this;

			$ionicLoading.show({
				template: 'Loading acomodation'
			});

			//path to event's acomodation
			var path = firebaseURL + '/acomodation';
			//sync to firebase event's acomodation
			var sync = new Firebase(path);
			var syncFilter = sync.orderByChild('event').equalTo($stateParams.idEvent);
			//data binding
			ctrl.acomodation = $firebaseArray(syncFilter);

			//check object has been loaded
			ctrl.acomodation
				.$loaded()
				.then(function(data){
					$ionicLoading.hide();
				});

			ctrl.edit = function(id) {
				$state.go('admin.acomodationEdit',{
					idEvent: $stateParams.idEvent,
					idAcomodation: id
				});
			}

			ctrl.add = function() {
				$state.go('admin.acomodationAdd', {
					idEvent: $stateParams.idEvent
				});
			}
		}
	])
