angular
	.module('acomodation')

	.controller('acomodationAddCtrl',[
		'$stateParams','firebaseURL', '$firebaseArray', '$ionicPopup', '$ionicLoading',
		function acomodationAddCtrl ($stateParams, firebaseURL, $firebaseArray, $ionicPopup, $ionicLoading) {
			// body...
			var addCtrl = this;

			/*
				Firebase
				ref to acomodation array
			*/
			var path = firebaseURL + 'acomodation';
			var sync = new Firebase(path);

			//scope array (three way data binding)
			addCtrl.acomodation = $firebaseArray(sync);

			/*
				Sending data:
				1. 	send data to acomodation array

			*/

			//to manage data
			addCtrl.formData = {};

			addCtrl.add = function() {
				//save data in acomodation array
		        addCtrl.acomodation
		        	.$add({
						name: addCtrl.formData.name,
						date: addCtrl.formData.date,
						price: addCtrl.formData.price,
						url: addCtrl.formData.url,
						lat: addCtrl.formData.lat,
						long: addCtrl.formData.long,
						zoom: addCtrl.formData.zoom,
						event: $stateParams.idEvent
		        	})
		        	.then(function(ref) {
		        		//clear acomodation
		        		addCtrl.formData = {};
		        		//say admin acomodation was created
			        	$ionicPopup.alert({
	     					title: 'acomodation Added',
	     					template: 'you can close this window'
	   					});
		        	});
			}//end add function
		}
	])

