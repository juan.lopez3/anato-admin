angular
	.module('cpd')

	.controller('cpdCtrl',[
		'$state', '$stateParams', '$firebaseObject', 'firebaseURL', '$ionicLoading',
		function ($state, $stateParams, $firebaseObject, firebaseURL, $ionicLoading) {
			var ctrl = this;

			$ionicLoading.show({
				template: 'Loading cpd'
			});

			//path to event's cpd
			var path = firebaseURL + '/cpd';
			//sync to firebase event's cpd
			var sync = new Firebase(path);
			//data binding
			ctrl.cpd = $firebaseObject(sync);

			//check object has been loaded
			ctrl.cpd
				.$loaded()
				.then(function(data){
					$ionicLoading.hide();
				});

			ctrl.save = function() {
				$ionicLoading.show({
					template: 'saving cpd data'
				});

				ctrl.cpd
					.$save()
					.then(function(){
						$ionicLoading.hide();
					},
					function(error){
						console.log(error);
					});
			}
		}
	])
