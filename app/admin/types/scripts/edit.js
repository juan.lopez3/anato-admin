var types = angular.module("types");

types.controller("typesEditCtrl", function($stateParams, firebaseURL, $ionicPopup, $ionicLoading, $localStorage, $common){
    var scope = this;
    $ionicLoading.show();

    scope.typeEvent = $stateParams.idType;
    scope.common = $common;
    scope.common.bodyClass = 'type-edit';

    var firebase = new Firebase(firebaseURL);
    var modules = new Firebase(firebaseURL).child("modules");
    var eventTypes = new Firebase(firebaseURL).child('eventTypes').child($stateParams.idType);

    scope.type = {};

    eventTypes.on("value", function(dataType) {
        scope.modules = dataType.val().modules || {};
        scope.modules.disponibles = scope.modules.disponibles || [];
        scope.modules.menu = scope.modules.menu || [];
        scope.modules.tabs = scope.modules.tabs || [];

        modules.on("value", function(response) {
            response.forEach(function(data){
                var item = data.val();
                item.id = data.key();

                var check = false;

                if(!scope.modules.menu && !scope.modules.tabs){
                    scope.modules.disponibles.push(item);
                    return 0;
                }

                angular.forEach(scope.modules.menu, function(value) {
                    if(item.id == value.id) check = true;
                });

                angular.forEach(scope.modules.tabs, function(value) {
                    if(item.id == value.id) check = true;
                });

                if(!check)
                    scope.modules.disponibles.push(item);

            });
            $ionicLoading.hide();
        });
    });

    scope.save = function () {
        $ionicLoading.show();
        angular.forEach(scope.modules, function(items, key) {
            angular.forEach(items, function(item, id) {
                delete scope.modules[key][id].$$hashKey;
            });
        });

        eventTypes.child('modules').set({tabs: scope.modules.tabs, menu: scope.modules.menu}, function (error) {
            if(error) {
                $ionicLoading.hide();
                console.log('error:', error);
            } else $ionicLoading.hide();
        });
    }

});

/*
    Filtro para ordenar objetos
-------------------------------------------------- */
types.filter('orderObjectBy', function() {
    return function(items, field, reverse) {
        var filtered = [];
        angular.forEach(items, function(item) {
            filtered.push(item);
        });
        filtered.sort(function (a, b) {
            return (a[field] > b[field] ? 1 : -1);
        });
        if(reverse) filtered.reverse();
        return filtered;
    };
});
