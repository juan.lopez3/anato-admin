var types = angular.module("types", []);

types.config(function($stateProvider, $urlRouterProvider) {

    $stateProvider
    /*
	  	Event Types
	  --------------------------------------------------------- */
        .state('admin.types', {
        url: '/types',
        views: {
            'adminView': {
                templateUrl: 'app/admin/types/views/index.tpl.html'
            }
        }
    })
        .state('typesEdit', {
        url: '/typesEdit/:idType',
        templateUrl: 'app/admin/types/views/edit.tpl.html'
    })

    /*
        .state('types', {
        url: '/types',
        views: {
            'adminView': {
                templateUrl: 'app/admin/types/views/index.tpl.html'
            }
        }
    })
        .state('typesAdd', {
        url: '/typesAdd',
        views: {
            'adminView': {
                templateUrl: 'app/admin/types/views/add.tpl.html'
            }
        }
    })
        .state('typesEdit', {
        url: '/typesEdit/:idType',
        views: {
            'adminView': {
                templateUrl: 'app/admin/types/views/edit.tpl.html'
            }
        }
    })*/
})
