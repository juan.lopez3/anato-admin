var types = angular.module("types");

types.controller("typesAddCtrl", function(firebaseURL, $ionicPopup){
  var scope = this;
  var firebase = new Firebase(firebaseURL);

  scope.typeEvent = {};
  scope.typeEvent.modules = [];
  scope.modules = [];

  firebase.child("modules").on("value", function(response) {
    angular.forEach(response.val(), function(value, key) {
      var item = { 'id': key, 'name': value.name, 'selected': false, 'location': 'menu' };
      scope.modules.push(item);
    });
  });

  scope.preview = function(){

  }

  scope.add = function(){
    angular.forEach(scope.modules, function(item){
      if(item.selected)
        console.log(item.id, item.name);
    });
  }

  /*
  Orden modulos
  -------------------------------------------------- */
  scope.moveItem = function(item, fromIndex, toIndex) {
    //Move the key in the array
    scope.modules.splice(fromIndex, 1);
    scope.modules.splice(toIndex, 0, item);
  };

});
