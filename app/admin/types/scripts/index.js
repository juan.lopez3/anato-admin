var types = angular.module("types")

types.controller("typeIndexCtrl", function($state, $ionicLoading, $ionicPopup, firebaseURL) {
  var scope = this;

  scope.types = {};
  scope.events = [];

  var firebase = new Firebase(firebaseURL);

  var types = firebase.child('eventTypes');

  types.on("value", function(datasnap){
    scope.types = datasnap.val();
  });

  /*
    Crear tipo de evento - popup de confirmación
  -------------------------------------------------- */
  scope.newType = function() {
    $ionicPopup.prompt({
      title: "EVENT TYPE's NAME:"
    }).then(function(res) {
      if(!res)
        return;

      $ionicLoading.show({template: '<ion-spinner icon="ripple"></ion-spinner>'});
      var key = types.push({name: res, events: {active: false}}).key();
      //$state.go("admin.typesEdit", {idType: key});
      $ionicLoading.hide();
    });
  }

  /*
    Borrar tipo de evento
  -------------------------------------------------- */
  scope.remove = function (record, key) {
    $ionicPopup.confirm({
      title: "Please Confirm:",
      template: 'Do you want to remove this event Type?'
    }).then(function(res) {
      if(!res)
        return;

      if (record.events.active) {
        $ionicPopup.alert({
          title: 'Warning!',
          template: "You can't remove an event type that has an associated event."
        });
        return;
      } else {
        types.child(key).remove();
      }
    });
  };

  /*
    Editar tipo de evento
  -------------------------------------------------- */
  scope.editType = function(idType){
    $state.go("admin.events", {idType: idType});
  };

});
