angular
	.module('documents')

  .controller('indexDocsCtrl', ['$state', '$scope', '$window', 'firebaseURL', '$firebaseArray', '$ionicLoading', '$ionicPopup', 'Upload', '$http', 'SERVER_FILES',
  function ($state, $scope, $window, firebaseURL, $firebaseArray, $ionicLoading, $ionicPopup, Upload, $http, SERVER_FILES) {

		$scope.document = {};

		$scope.arrayDocs = $firebaseArray(new Firebase(firebaseURL + "documents"));

		$scope.arrayDocs.$loaded().then(function(response) {
			$scope.documents = response;
			$ionicLoading.hide();
		});


		$scope.addDoc = function(files){
			if (!files || files[0] === undefined){ return; };

			Upload.upload({
        url: SERVER_FILES,
				file: files[0],
				fields: { operation: 'insert' }
			})
			.progress(function (evt) {
				$ionicLoading.show({ template: 'Loading... ' + parseInt(100 * evt.loaded / evt.total) });
			})
			.success(function (data, status, headers, config) {
				if (data === 'pdf') {
            		$ionicPopup.alert({ title: '<b>Error</b>', template: 'File(s) must be PDF.' });
            		return;

            	} else if (data === 'server') {
            		$ionicPopup.alert({ title: 'Error', template: 'There was a problem loading files, try again.' });
            		return;
            	}

            	$scope.document.url = data;
            	$ionicLoading.hide();

            	$ionicPopup.show({
                	template: '<input type="text" ng-model="document.name">',
                	title: 'Set the filename',
                	scope: $scope,
                	buttons:[
                		{
                			text: 'Accept',
                			type: 'button-positive',
                			onTap: function(e){
                				return;
                			}
                		}
                	]
                })
                .then(function(response) {
                	$scope.document.name = ($scope.document.name === undefined) ? 'Untitled' : $scope.document.name;
                	$scope.arrayDocs.$add($scope.document).then(function(ref){});

                });

			});

		}

		$scope.editName = function(doc){
			$scope.doc = doc;

			$ionicPopup.show({
            	template: '<input type="text" ng-model="doc.name">',
            	title: 'Set the new filename',
            	scope: $scope,
            	buttons:[
            		{
            			text: 'Accept',
            			type: 'button-positive',
            			onTap: function(e){
            				return;
            			}
            		}
            	]
            })
            .then(function(response) {
            	$scope.arrayDocs.$save($scope.doc).then(function(ref){});

            });

		}

		$scope.downloadDoc = function(urlfile){
			var a = document.createElement("A");
			a.href = urlfile;
			a.download = "";
			a.click();
		}

		$scope.deleteDoc= function(doc){
			$ionicPopup.confirm({
				title: 'Alert',
				template: 'Are you sure you want delete this file?'
			})
			.then(function(response){
				if( !response ){ return; }
				$scope.arrayDocs.$remove(doc);
			});
		}

		$scope.viewDoc = function(doc){
          $window.open(doc.url, '_blank');
		}

	}])

	.controller('viewDocCtrl', ['$state', '$stateParams', '$scope', 'PDFViewerService', function ($state, $stateParams, $scope, pdf) {
		$scope.doc = pdf.Instance("viewer");

		$scope.pdfName = 'Contrato';
		$scope.pdfURL = $stateParams.url;

		$scope.pageLoaded = function(curPage, totalPages) {
			$scope.currentPage = curPage;
			$scope.totalPages = totalPages;
		};

		$scope.loadProgress = function(loaded, total, state) {
			$scope.pages = total;
		};

		$scope.goTo = function(){
			console.log("goto");
		}


	}])
