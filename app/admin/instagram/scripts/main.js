angular.module("instagram", [])

.controller("instaCtrl", function($stateParams, $scope, $http, $ionicLoading, firebaseURL){

  var firebase = new Firebase(firebaseURL).child("events").child($stateParams.idEvent).child("instagram");

  var feed = null;

  var before = function(){
    $ionicLoading.show({template: "<ion-spinner icon='spiral'></ion-spinner>"});
  }

  var after = function(){
    $ionicLoading.hide();
  }

  $scope.getByHashtag = function(hashtag){
    feed = new Instafeed({
      target: "instagram",
      clientId: "91bd5285f5914748ae8ee7ac02707f1d",
      get: 'tagged',
      tagName: hashtag,
      template: "<img src='{{image}}' class='instaphoto' />",
      limit: 4,
      resolution: "low_resolution",
      before: before(),
      after: after()
    });

    feed.run();
  }

  $scope.getByUser = function(userId){
    feed = new Instafeed({
      target: "instagram",
      clientId: "91bd5285f5914748ae8ee7ac02707f1d",
      accessToken: "1547019221.91bd528.8144cf51806f42aa86cda069326aedc7",
      get: 'user',
      userId: userId,
      template: "<img src='{{image}}' class='instaphoto' />",
      limit: 4,
      resolution: "low_resolution",
      before: before(),
      after: after()
    });

    feed.run();
  }

  firebase.on("value", function(response){
    document.getElementById('instagram').innerHTML = '';

    $scope.text = response.val();

    if ($scope.text.substring(0, 1) == '#' && $scope.text.substring(1) != "") {
      $scope.getByHashtag($scope.text.substring(1));
    } else if ($scope.text.substring(0, 1) == '@' && $scope.text.substring(1) != "") {

      $http.get("http://services-anato.rhcloud.com/instafeed.php?instauser=" + $scope.text.substring(1))
      .then(function(data){
        $scope.getByUser(parseInt(data.data));
      });

    }

  });

  $scope.change = function(hashtag) {
    firebase.set(hashtag);
  }

  $scope.more = function(){
    feed.next();
  }

})
