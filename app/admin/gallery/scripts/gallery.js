angular.module("gallery")

  .controller("galleryCtrl", function($state, $stateParams, firebaseURL, Upload, SERVER_IMAGES, $ionicLoading){
  var scope = this;

  var event = new Firebase(firebaseURL).child("events").child($stateParams.idEvent);
  var gallery = new Firebase(firebaseURL).child("gallery");

  event.on("value", function(data){
    scope.event = data.val();

    if(!scope.event.gallery){
      event.child("gallery").set(gallery.push(true).key());
    } else {
      gallery.child(scope.event.gallery).on("value", function(snapshot){
        scope.gallery = snapshot.val();
      });
    }

  });

  scope.addImages = function(images){
    if(!images || images.length == 0)
      return;

    for(var i = 0; i < images.length; i++){
      Upload.upload({
        url: SERVER_IMAGES,
        file: images[i]
      }).then(function(response){
        gallery.child(scope.event.gallery).push({title: "Title", img: response.data});
        $ionicLoading.hide();

      }, function(error){
        console.log('Error:', error);

      }, function(time){
        $ionicLoading.show({template: "Loading " + parseInt(100.0 * time.loaded / time.total) + "%"})

      });
    }
  }

  scope.saveTitle = function(key){
    gallery.child(scope.event.gallery).child(key).set(scope.gallery[key]);
  }

  scope.deleteImage = function(key){
    gallery.child(scope.event.gallery).child(key).remove();
  }

});
