angular
	.module('sponsors')

	.controller('sponsorsEditCtrl',[
  '$stateParams', '$ionicLoading', '$firebaseObject', '$firebaseArray', 'firebaseURL', 'Upload', '$ionicPopup', '$scope', 'SERVER_IMAGES', 'SERVER_FILES',
  function ($stateParams, $ionicLoading, $firebaseObject, $firebaseArray, firebaseURL, Upload, $ionicPopup, $scope, SERVER_IMAGES, SERVER_FILES) {

		var ctrl = this;

		//show loading while data it's retrieve
		$ionicLoading.show({
			template: 'retrieving sponsor data'
		});

		//path to selectec programme
		var path = firebaseURL + '/sponsors/' + $stateParams.idSponsor;
		//sync to firebase event's schedule
		var sync = new Firebase(path);
		//three way data binding
		ctrl.sponsor = $firebaseObject(sync);

		//wait for data load
		ctrl.sponsor.$loaded().then(function(data){
			$ionicLoading.hide();
			checkDocs(data);
		});

		ctrl.events = $firebaseArray(new Firebase(firebaseURL + "events"));

		function checkDocs(data) {
			/* get documents in array */
			ctrl.documents = ctrl.documents || [];
			angular.forEach(data.documents, function(id) {
				ctrl.documents.push( $firebaseObject( new Firebase(firebaseURL+'documents/'+id) ) );
			});
		}

		ctrl.saveSponsor = function() {
			$ionicLoading.show({
				template: 'saving sponsor data'
			});

			ctrl.sponsor
				.$save()
				.then(function(){
				$ionicLoading.hide();
			},
					  function(error){
				console.log(error);
			});
		}

		ctrl.image = function(files, model){
			if (!files || files[0] == undefined){ return; }

			$ionicLoading.show({ template: 'Uploading Image ...' });

			var file = files[0];
			Upload.upload({
        url: SERVER_IMAGES,
				file: file
			})
				.progress(function (evt) {})
				.success(function (data, status, headers, config) {
				ctrl.sponsor[model] = data;
				$ionicLoading.hide();

			}).error(function (data, status, headers, config) {
				console.log('Error status: ' + status);
			});
		};

		ctrl.deleteImg = function(model) {
			ctrl.sponsor[model] = '';
		};

		/*
			Documents
		*/
		ctrl.uploadfiles = function(files){
			if (!files || files[0] === undefined){ return; };

			ctrl.documents = ctrl.documents || [];

			angular.forEach(files, function(file) {
				Upload.upload({
          url: SERVER_FILES,
					file: file,
					fields: { operation: 'insert' }
				})
					.progress(function (evt) {
					$ionicLoading.show({ template: 'Loading... ' + parseInt(100 * evt.loaded / evt.total) });
				})
					.success(function (data, status, headers, config) {
					if (data === 'pdf') {
						$ionicPopup.alert({ title: '<b>Error</b>', template: 'File(s) must be PDF or PPT.' });
						$ionicLoading.hide();
						return;

					} else if (data === 'server') {
						$ionicPopup.alert({ title: 'Error', template: 'There was a problem loading files, try again.' });
						$ionicLoading.hide();
						return;
					}

					$ionicLoading.hide();

					$scope.document = {};

					$ionicPopup.show({
						template: '<input type="text" ng-model="document.name">',
						title: 'Set the filename',
						scope: $scope,
						buttons:[
							{
								text: 'Accept',
								type: 'button-positive',
								onTap: function(e){
									return;
								}
							}
						]
					})
						.then(function(response) {
						var name = ($scope.document.name === undefined) ? 'Untitled' : $scope.document.name;

						var documents = $firebaseArray(new Firebase(firebaseURL + "documents"));
						documents.$loaded();
						documents.$add({
							url: data,
							name: name
						}).then(function (response) {
							ctrl.sponsor.documents = ctrl.sponsor.documents || [];
							ctrl.sponsor.documents.push(response.key());
							ctrl.sponsor.$save().then(function() {
								reloadDocs();
								$ionicLoading.hide();
							});
						});
					});

				});

			});

		};//upload pdf end

		ctrl.docEdit = function(document){
			$scope.document = {name: document.name};

			$ionicPopup.show({
				template: '<input type="text" ng-model="document.name">',
				title: 'Set the filename',
				scope: $scope,
				buttons:[
					{
						text: 'Accept',
						type: 'button-positive',
						onTap: function(e){
							return;
						}
					}
				]
			})
				.then(function(response) {
				document.name = ($scope.document.name === undefined) ? 'Untitled' : $scope.document.name;
				document.$save().then(reloadDocs());
			});
		};

		ctrl.docDelete = function(document){
			$ionicPopup.confirm({
				title: 'Alert',
				template: 'Are you sure you want delete this file?'
			})
				.then(function(response){
				if( !response ){ return; }

				$firebaseObject( new Firebase(firebaseURL+'documents/'+document.$id) ).$remove();
				ctrl.sponsor.documents.splice(ctrl.sponsor.documents.indexOf(document.$id), 1);
				ctrl.sponsor.$save().then(reloadDocs());
			});
		};

		function reloadDocs() {
			ctrl.documents = [];
			angular.forEach(ctrl.sponsor.documents, function(id) {
				ctrl.documents.push( $firebaseObject( new Firebase(firebaseURL+'documents/'+id) ) );
			});
		}
	}
])
