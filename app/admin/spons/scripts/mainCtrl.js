var spn = angular.module('sponsors');

spn.controller('sponsorsCtrl',[
	'$scope', '$state', '$stateParams', '$firebaseArray', 'firebaseURL', '$ionicLoading', '$ionicPopup',
	function ($scope, $state, $stateParams, $firebaseArray, firebaseURL, $ionicLoading, $ionicPopup) {
		var ctrl = this;

		$ionicLoading.show({
			template: 'Loading sponsors'
		});

		//path to event's sponsors
		var path = firebaseURL + '/sponsors';
		//sync to firebase event's sponsors
		var sync = new Firebase(path);
		//data binding
		ctrl.sponsors = $firebaseArray(sync);

		//check object has been loaded
		ctrl.sponsors.$loaded().then(function(data){
			$ionicLoading.hide();
		});

		ctrl.edit = function(id) {
			$state.go('admin.sponsorsEdit',{
				idSponsor: id
			});
		};

		/*
    Crear Spon - popup de confirmación
		-------------------------------------------------- */
		ctrl.newSpon = function() {
			$scope.SponObj = {}/*1*/
			var newSponPopup = $ionicPopup.confirm({
				title: "COMPANY NAME",
				template: '<input type="text" ng-model="SponObj.company">',
				scope: $scope
			});
			newSponPopup.then(function(res) {
				if(res) {
					ctrl.newSponConfirm($scope.SponObj);
				} else {
					console.log('You are not sure');
				}
			});
		};

		/*
    Crear Spon  - enviar dato a firebase
  	-------------------------------------------------- */
		ctrl.newSponConfirm = function(data) {
			$ionicLoading.show({template: '<ion-spinner icon="ripple"></ion-spinner>'});
			ctrl.sponsors.$add(data).then(function(ref) {
				var idSpon = ref.key();
				$state.go("admin.sponsorsEdit", {idSponsor: idSpon});
			});
		};

		/*
    Borrar spon
  	-------------------------------------------------- */
		ctrl.remove = function (record) {
			var newEventPopup = $ionicPopup.confirm({
				title: "Please Confirm:",
				template: 'Do you want to remove this sponsor?',
				scope: $scope
			});
			newEventPopup.then(function(res) {
				if(res) {
					//esta linea borra el tipo de evento
					ctrl.sponsors.$remove(record);
				} else {
					console.log('You are not sure');
				}
			});
		};


	}
])
