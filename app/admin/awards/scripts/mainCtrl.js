angular
	.module('awards')

	.controller('awardsCtrl',[
		'$state', '$stateParams', '$firebaseArray', 'firebaseURL', '$ionicLoading',
		function ($state, $stateParams, $firebaseArray, firebaseURL, $ionicLoading) {
			var ctrl = this;

			$ionicLoading.show({
				template: 'Loading awards'
			});

			//path to event's awards
			var path = firebaseURL + '/awards';
			//sync to firebase event's awards
			var sync = new Firebase(path);
			//data binding
			ctrl.awards = $firebaseArray(sync);

			//check object has been loaded
			ctrl.awards
				.$loaded()
				.then(function(data){
					$ionicLoading.hide();
				});

			ctrl.edit = function(id) {
				$state.go('admin.awardsEdit',{
					idAward: id
				});
			}
		}
	])
