angular
	.module('awards')

	.controller('awardAddCtrl',[
		'firebaseURL', '$firebaseArray', '$ionicPopup', '$ionicLoading', 'Upload',
		function awardsAddCtrl (firebaseURL, $firebaseArray, $ionicPopup, $ionicLoading, Upload) {
			// body...
			var addCtrl = this;

			/*
				Firebase
				ref to awards array
			*/
			var path = firebaseURL + 'awards';
			var sync = new Firebase(path);

			//scope array (three way data binding)
			addCtrl.awards = $firebaseArray(sync);

			/*
				Sending data:
				1. 	send data to awards array

			*/

			//to manage data
			addCtrl.award = {};

			addCtrl.add = function() {
				//save data in awards array
		        addCtrl.awards
		        	.$add({
						title: addCtrl.award.title,
						content: addCtrl.award.content,
						sponsor: addCtrl.award.sponsor
		        	})
		        	.then(function(ref) {
		        		//clear awards
		        		addCtrl.awards = {};
		        		//say admin awards was created
			        	$ionicPopup.alert({
	     					title: 'award Added',
	     					template: 'you can close this window'
	   					});
		        	});
			}//end add function

			var sponsorsRef = new Firebase(firebaseURL).child('sponsors');
			var sponsorsRefFilter = sponsorsRef.orderByChild('awardSponsor').equalTo(true);
			addCtrl.sponsors = $firebaseArray(sponsorsRef);
		}
	])

