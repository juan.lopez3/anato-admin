angular
	.module('awards')

	.controller('awardEditCtrl',[
		'$stateParams', '$ionicLoading', '$firebaseObject', 'firebaseURL', '$firebaseArray',
		function ($stateParams, $ionicLoading, $firebaseObject, firebaseURL, $firebaseArray) {

			var ctrl = this;

			//show loading while data it's retrieve
			$ionicLoading.show({
				template: 'retrieving award data'
			});

			//path to selectec programme
			var path = firebaseURL + '/awards/' + $stateParams.idAward;
			//sync to firebase event's schedule
			var sync = new Firebase(path);
			//three way data binding
			ctrl.award = $firebaseObject(sync);

			//wait for data load
			ctrl.award
				.$loaded()
				.then(function(data){
					$ionicLoading.hide();
				});

			ctrl.saveAward = function() {
				$ionicLoading.show({
					template: 'saving award data'
				});

				ctrl.award
					.$save()
					.then(function(){
						$ionicLoading.hide();
					},
					function(error){
						console.log(error);
					});
			}

			var sponsorsRef = new Firebase(firebaseURL).child('sponsors');
			var sponsorsRefFilter = sponsorsRef.orderByChild('awardSponsor').equalTo(true);
			ctrl.sponsors = $firebaseArray(sponsorsRefFilter);
		}
	])
