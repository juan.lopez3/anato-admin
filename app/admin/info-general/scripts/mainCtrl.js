angular
	.module('infoGeneral')

	.controller('infoGeneralCtrl',[
	'$state', '$stateParams', '$firebaseObject', 'firebaseURL', '$ionicLoading',
	function ($state, $stateParams, $firebaseObject, firebaseURL, $ionicLoading) {
		var ctrl = this;

		$ionicLoading.show({
			template: 'Loading ...'
		});

		//path to event's infoGeneral
		var path = firebaseURL + 'infoGeneral/' + $stateParams.idEvent;
		//sync to firebase event's infoGeneral
		var sync = new Firebase(path);
		//data binding
		ctrl.infoGeneral = $firebaseObject(sync);

		//check object has been loaded
		ctrl.infoGeneral.$loaded().then(function(data){
			$ionicLoading.hide();
		});

		ctrl.save = function() {
			$ionicLoading.show({
				template: 'saving data'
			});

			ctrl.infoGeneral.$save().then(function(){
				$ionicLoading.hide();
			},function(error){
				console.log(error);
			});
		}
	}
])
