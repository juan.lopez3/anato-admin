var adm = angular.module('admin');

adm.controller('adminCtrl', function ($state, $scope, firebaseURL, $firebaseObject, $firebaseArray, $ionicPopup) {

	var version = $firebaseObject(new Firebase(firebaseURL + "version"));
    var events = new Firebase(firebaseURL + "events/order");

	$scope.update = function(){
		version.$value += 1;
		version.$save();
        
        events.once('value', function (snap) {
          var order = [];
          snap.forEach(function (event) {
            order.push(event.val());
          });
          events.set(order);
        });

		$ionicPopup.alert({
			title: 'Alert',
			template: 'The version is updated',
			okText: 'OK'
		});
	}

  $scope.logout = function () {
    console.log("entro logout");
    new Firebase(firebaseURL).unauth();
    $state.go("login");
  }

})

adm.controller('adminMenuCtrl', function ($state, $scope, firebaseURL) {
	//menu items
	$scope.adminMenuItems = _adminMenuItems;

	//routing function
	$scope.goToLink = function(where) {
		console.log(where);
	}

});

var _adminMenuItems = [
	{
		name: 'Eventos',
		link: '#/admin/types'
	},
	{
		name: 'Ediciones Activas',
		link: '#/admin/ediciones'
	},
	{
		name: 'Speakers',
		link: '#/admin/speakers'
	},
	{
		name: 'Sponsors',
		link: '#/admin/sponsors'
	},
  {
    name: 'Terms',
    link: '#/admin/terms'
  },
  {
    name: 'Push Notifications',
    link: "#/admin/notifications"
  },
  {
    name: 'Settings',
    link: '#/admin/settings'
  },
  {
    name: 'Usuarios',
    link: "#/admin/edit-event/users"
  }

];
