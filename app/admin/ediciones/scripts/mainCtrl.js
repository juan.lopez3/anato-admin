var ediciones = angular.module('ediciones');

ediciones.controller('edicionesCtrl', function (firebaseURL) {

  var scope = this;

  scope.events = [];

  var firebase = new Firebase(firebaseURL);

  var types = firebase.child('eventTypes');
  var events = firebase.child("events");

  events.child("order").once("value", function (editions) {
    editions.forEach(function (edition) {
      events.child(edition.val()).once("value", function (event) {
        var obj = event.val();
        obj.key = edition.val();
        scope.events.push(obj);
      });
    });
  });

  scope.moveItem = function (item, fromIndex, toIndex) {
    scope.events.splice(fromIndex, 1);
    scope.events.splice(toIndex, 0, item);

    var keys = [];

    angular.forEach(scope.events, function (event) {
      keys.push(event.key);
    });

    events.child("order").set(keys);
  };

});