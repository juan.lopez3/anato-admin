var ediciones = angular.module('ediciones', []);

ediciones.config(function($stateProvider, $urlRouterProvider) {

	$stateProvider
	/*
	  	EDICIONES
	  --------------------------------------------------------- */
		.state('admin.ediciones', {
		url: '/ediciones',
		views: {
			'adminView': {
				templateUrl: 'app/admin/ediciones/views/index.tpl.html'
			}
		}
	})
});
