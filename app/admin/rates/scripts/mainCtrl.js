angular
	.module('rates')

	.controller('ratesCtrl',[
	'$state', '$stateParams', '$firebaseArray', '$firebaseObject', 'firebaseURL', '$ionicLoading', 'Upload', 'SERVER_IMAGES',
	function ($state, $stateParams, $firebaseArray, $firebaseObject, firebaseURL, $ionicLoading, Upload, SERVER_IMAGES) {
		var ctrl = this;

		$ionicLoading.show();
		/*
			TRAER ARRAY DE TARIFAS
		------------------------------------------------------------------ */
		//path to event's rates
		var pathList = firebaseURL + 'rates/' + $stateParams.idEvent + '/list';
		//data binding
		ctrl.rates = $firebaseArray(new Firebase(pathList));
		//check object has been loaded
		ctrl.rates.$loaded().then(function(data){
				$ionicLoading.hide();
		});
		/*
			GUARDAR TARIFA MODIFICADO
		------------------------------------------------------------------ */
		ctrl.save = function(index) {
			$ionicLoading.show();
			ctrl.rates.$save(index).then(function(){
				ctrl.edit = '';
				$ionicLoading.hide();
			},function(error){
				console.log(error);
			});
		};
		/*
			AGREGAR NUEVO TARIFA AL LISTADO
		------------------------------------------------------------------ */
		ctrl.data = {};//object to store temp data
		ctrl.crear = function () {
			$ionicLoading.show();
			ctrl.rates.$add(ctrl.data).then(function(){
				//si se enviaron los datos limpiamos el formulario
				ctrl.data = {};
				$ionicLoading.hide();
			}, function(error){
				$ionicLoading.hide();
				console.log(error);
			});
		};
		/*
			ELIMINAR TARIFA DEL LISTADO
		------------------------------------------------------------------ */
		ctrl.remove = function(item) {
			$ionicLoading.show();
			ctrl.rates.$remove(item).then(function(){
				$ionicLoading.hide();
			});
		};

	}
])
