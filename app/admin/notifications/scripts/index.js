angular.module("notifications", [])

  .controller("notificationsCtrl", function(firebaseURL, $http, $ionicPopup, $ionicLoading){
  var scope = this;

  var firebase = new Firebase(firebaseURL + "/notifications");

  firebase.on("value", function(data){
    scope.notifications = data.val();
  });

  scope.send = function(){
    if(scope.title == undefined || scope.message == undefined){
      $ionicPopup.alert({ title: 'Alerta', template: 'Por favor, escriba el título y el mensaje de la notificación.' });
      $ionicLoading.hide();
      return;
    }

    $ionicPopup.confirm({
      title: "Está seguro?",
      template: '<br><div class="navbar navbar-default"><p class="navbar-text"><strong>' + scope.title + '</strong><br>' + scope.message + '</p></div>'
    }).then(function(response){
      if(response){
        $http.get("http://pushnotifications-anato.rhcloud.com/AdminCMS?title=" + scope.title + "&msg=" + scope.message);
        firebase.push({ title: scope.title, message: scope.message, date: Date.now() });
        scope.title = ""; scope.message = "";
        $ionicPopup.alert({ title: 'Alert', template: 'Notificaciones enviadas correctamente.' });
      }

    });

  }

})
