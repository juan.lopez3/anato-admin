// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var app = angular.module('common', [
  'ionic',
  'config',
  'ionic-datepicker',
  'dndLists',
  'textAngular', //text editor / Wysiwyg
  'firebase', //Libreria FB
  'ngPDFViewer', //Libreria para visualizar pdfs
  'ngStorage',
  'login',
  'admin'
  ]);

app.constant('firebaseURL', 'https://anatoapp.firebaseio.com/');
app.constant('SERVER_FILES', 'http://services-anato.rhcloud.com/files.php');
app.constant('SERVER_IMAGES', 'http://services-anato.rhcloud.com/images.php');
app.constant('SERVER_USERS', 'http://services-anato.rhcloud.com/users.php');
app.constant('SERVER_EXHIBITORS', 'http://services-anato.rhcloud.com/exhibitors.php');

app.run(function ($ionicPlatform, $state, firebaseURL) {

  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }

    var login = new Firebase(firebaseURL).getAuth();

    if(!login)
      $state.go("login");

  });
});

app.config(function($stateProvider, $urlRouterProvider, $sceDelegateProvider) {
  $sceDelegateProvider.resourceUrlWhitelist(['mocion', 'http://mocionsoft.com/**' ]);
});

app.controller('commonCtrl', function($common){
  var scope = this;
  scope.common = $common;
});
