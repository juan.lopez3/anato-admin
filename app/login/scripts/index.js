angular.module("login", [])

.controller("loginCtrl", function($state, firebaseURL){
  var scope = this;

  scope.login = function(){
    var firebase = new Firebase(firebaseURL);

    firebase.child("administrators").on("value", function (data) {
      data.forEach(function (response) {
        if(response.val() === scope.user){
          firebase.authWithPassword({"email": scope.user, "password": scope.password},
            function(error, authData){
              if (error)
                console.log('error:', error);
              else
                $state.go("admin.types");
          });
        }
      });

    });


  }

})
