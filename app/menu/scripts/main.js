angular.module('common')

    .directive('mainMenu', function () {
    return {
        restrict: 'E',
        templateUrl: 'app/menu/views/menu.tpl.html',
        controller: function ( $firebaseObject, firebaseURL) {
            //login here
            this.version = $firebaseObject( new Firebase( firebaseURL + 'version' ) );

            this.update = function() {
                this.version.$value += 1;
                this.version.$save();
            };
        },
        controllerAs: 'menuCtrl'
    };
});
